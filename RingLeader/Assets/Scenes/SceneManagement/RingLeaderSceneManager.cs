﻿/*
Author: John Hartzell
Date Modified: 7/26/17
Credit:
Purpose: This is a static scene manager script that can be called from any class in the game.
Worked on by: Eduardo Sarmiento.
*/

using UnityEngine.SceneManagement;
using UnityEngine;

public class RingLeaderSceneManager : MonoBehaviour {

    public static RingLeaderSceneManager instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }

        Debug.Log(instance);
    }

    public void ChangeScene(int sceneID)
    {
        if(SceneManager.GetSceneByBuildIndex(sceneID) != null)
        {
            SceneManager.LoadScene(sceneID);
        }
    }

}
