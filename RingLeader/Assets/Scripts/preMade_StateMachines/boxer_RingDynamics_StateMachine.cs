﻿using System;
using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class boxer_RingDynamics_StateMachine : ByTheTale.StateMachine.MachineBehaviour {


    protected boxer_InnerSphere boxerInnerSphere;

    public override void AddStates()
    {
        AddState<InCornerState>();
        AddState<On_The_RailState>();
        AddState<Crossing_InnerSphere_State>();

        AddState<InnerSphere_Idol>();
    }


    public class boxer_InnerSphere: ByTheTale.StateMachine.State
    {

        protected boxer_InnerSphere boxerInnerSphere;


        public override void Enter()
        {
            boxerInnerSphere = GetMachine<boxer_RingDynamics_StateMachine>().GetComponent<boxer_InnerSphere>();
        }

    }

    //State for when the player is within one of the cornerTrigs

    public class InCornerState: boxer_InnerSphere
    {

        public override void Enter()
        {
            base.Enter();   
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }



    public class On_The_RailState : boxer_InnerSphere
    {

        public override void Enter()
        {
            base.Enter();
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }




    public class Crossing_InnerSphere_State : boxer_InnerSphere
    {

        public override void Enter()
        {
            base.Enter();
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }



    public class InnerSphere_Idol : boxer_InnerSphere
    {

        public override void Enter()
        {
            base.Enter();
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }

}
