﻿//using System;
using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class Boxer_outerSphere_StateMachine : ByTheTale.StateMachine.MachineBehaviour {


    protected Boxer_outerSphere boxerOuterSphere;

    public override void AddStates()
    {
        AddState<Crossing_OuterSphere_State>();
        AddState<InsideSphere_State>();
        AddState<OutsideSphere_State>();
    }


    public class Boxer_outerSphere : ByTheTale.StateMachine.State
    {

        protected Boxer_outerSphere boxerOuterSphere;


        public override void Enter()
        {
            boxerOuterSphere = GetMachine<Boxer_outerSphere_StateMachine>().GetComponent<Boxer_outerSphere>();
        }

    }

    //State for when the player is within one of the cornerTrigs

    public class OutsideSphere_State : Boxer_outerSphere
    {

        public override void Enter()
        {
            base.Enter();
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }



    public class InsideSphere_State : Boxer_outerSphere
    {

        public override void Enter()
        {
            base.Enter();
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }




    public class Crossing_OuterSphere_State : Boxer_outerSphere
    {

        public override void Enter()
        {
            base.Enter();
        }

        public override void Execute()
        {
            base.Execute();
        }

        public override void Exit()
        {
            base.Exit();
        }

    }

    
    
}
