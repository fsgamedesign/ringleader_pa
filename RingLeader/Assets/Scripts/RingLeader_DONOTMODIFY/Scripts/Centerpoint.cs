﻿//Date: 05/10/17
//Author: Timothy Cable (May, 17) / (continue adding authors as needed here.)
//Credits: John Hartzell for assistance with the lerp code.
//
//
//
//
//Purpose: To create and maintain a centerpoint between the two boxers to allow for referee and player commands that involve that center.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Centerpoint : MonoBehaviour
{
    //This is the Blue Boxer's Game Object and Positioning Data.
    [SerializeField]
    private GameObject boxerOneGO;
    private Vector3 boxerOnePos;

    //This is the Red Boxer's Game Object and Positioning Data.
    [SerializeField]
    private GameObject boxerTwoGO;
    private Vector3 boxerTwoPos;

    //This is the Center Point's Game Object and Positioning Data.
    [SerializeField]
    private GameObject centerPoint;
    private Vector3 centerpointPos;

    // Here we declare the initial values needed to track the data.
    void Start()
    {
        centerpointPos = centerPoint.transform.position;
        boxerOnePos = boxerOneGO.transform.position;
        boxerTwoPos = boxerTwoGO.transform.position;
    }
    void Update()
    {
        //Update the current position of the players constantly.
        boxerOnePos = boxerOneGO.transform.position;
        boxerTwoPos = boxerTwoGO.transform.position;
        //calculate the pos the centerpoint should be. Halfway between the two boxer GO's.
        centerpointPos = Vector3.Lerp(boxerOnePos, boxerTwoPos, 0.5f);
        //set the position.
        this.gameObject.transform.position = centerpointPos;
    }
}
