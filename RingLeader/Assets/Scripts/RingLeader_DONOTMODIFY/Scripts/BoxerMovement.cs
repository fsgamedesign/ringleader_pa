﻿//Contributors: Dang Nguyen, Tyler Fronczak

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class BoxerMovement : MonoBehaviour
{
    #region Instance Variables
    [Header("Stats")]
    public float moveSpeed;
    [SerializeField] float rotationSpeed;
    [SerializeField] float pushDistance;

    [Header("Input")]
    [SerializeField] string horizontalAxis;
    [SerializeField] string verticalAxis;
    [SerializeField] KeyCode leftInput;
    [SerializeField] KeyCode rightInput;
    [SerializeField] KeyCode toggleMoveMode;
    [SerializeField] KeyCode pushInput;

    [Header("Enemy")]
    [SerializeField] string enemyTag;
    GameObject enemyObj;
    bool isEnemyFound;

    RotationDirection rotationDirection;
    enum RotationDirection
    {
        Left,
        Right,
        None
    }

    MoveMode moveMode = MoveMode.Locked;
    enum MoveMode
    {
        Free,
        Locked,
        None
    }

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    bool isPushable = true;
    #endregion

    void Awake()
    {
        characterController = GetComponent<CharacterController>();
        AssignEnemy();
    }

    void Update()
    {
        HandleInput();
    }

    void FixedUpdate()
    {
        switch(moveMode)
        {
            case MoveMode.Free:
                HandleFreeMovement();
                HandleFreeRotation(rotationDirection);
                break;
            case MoveMode.Locked:
                transform.LookAt(enemyObj.transform);
                HandleLockedMovement();
                break;
            case MoveMode.None:
                return;
        }
    }

    void HandleInput()
    {
        if (Input.GetKey(leftInput))
        {
            rotationDirection = RotationDirection.Left;
        }
        else if (Input.GetKey(rightInput))
        {
            rotationDirection = RotationDirection.Right;
        }
        else
        {
            rotationDirection = RotationDirection.None;
        }

        if (Input.GetKeyDown(toggleMoveMode))
        {
            ToggleMoveMode();
        }

        if (Input.GetKeyDown(pushInput))
        {
            Push(enemyObj);
        }
    }

    void HandleFreeMovement()
    {
        moveDirection = new Vector3(0, 0, Input.GetAxis(verticalAxis));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, 0, transform.position.z); //Keeps the boxer's feet on the boxing canvas.
    }
    void HandleLockedMovement()
    {
        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, 0, transform.position.z); //Keeps the boxer's feet on the boxing canvas.
    }
    void ToggleMoveMode()
    {
        switch (moveMode)
        {
            case MoveMode.Free:
                moveMode = MoveMode.Locked;
                break;
            case MoveMode.Locked:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.None:
                return;
        }
    }

    void HandleFreeRotation(RotationDirection direction)
    {
        switch(direction)
        {
            case RotationDirection.Left:
                transform.Rotate(0.0f, -1 * rotationSpeed, 0.0f);
                break;
            case RotationDirection.Right:
                transform.Rotate(0.0f, 1 * rotationSpeed, 0.0f);
                break;
            case RotationDirection.None:
                return;
        }
    }

    /// <summary> Assigns the enemy object based upon tag comparision. Called at Awake(). </summary>
    void AssignEnemy()
    {
        if (gameObject.CompareTag("BlueBoxer")){
            enemyObj = GameObject.FindGameObjectWithTag("RedBoxer");
        } else if (gameObject.CompareTag("RedBoxer")){
            enemyObj = GameObject.FindGameObjectWithTag("BlueBoxer");
        } else {
            Debug.LogError(gameObject.name + ", needs to be tagged as RedBoxer or BlueBoxer");
        }
    }

    /// <summary> Currently only supports pushing an enemy boxer backwards. </summary>
    void Push(GameObject objToPush)
    {
        if (objToPush.GetComponent<BoxerMovement>())
        {
            BoxerMovement enemyBoxer = objToPush.GetComponent<BoxerMovement>();

            if (isEnemyFound)
            {
                if (enemyBoxer.isPushable)
                {
                    Vector3 back = -enemyBoxer.gameObject.transform.forward;
                    enemyBoxer.characterController.Move(back * pushDistance);
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(enemyTag))
        {
            isEnemyFound = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(enemyTag))
        {
            isEnemyFound = false;
        }
    }
}