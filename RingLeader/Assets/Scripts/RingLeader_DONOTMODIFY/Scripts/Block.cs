﻿//Contributors: Dang Nguyen, Tyler Fronczak

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (BoxerMovement))]
public class Block : MonoBehaviour
{
    [Header("Input")]
    [SerializeField] KeyCode highBlockInput;
    [SerializeField] KeyCode lowBlockInput;

    bool isBlocking;

    BlockState currentBlockState = BlockState.None;
    enum BlockState
    {
        High,
        Low,
        None
    }

	void Update ()
    {
        HandleInput();
	}

    void HandleInput()
    {
        if (Input.GetKey(highBlockInput))
        {
            ChangeBlockState(BlockState.High);
        }
        else if (Input.GetKey(lowBlockInput))
        {
            ChangeBlockState(BlockState.Low);
        }
        else
        {
            ChangeBlockState(BlockState.None);
        }
    }

    void ChangeBlockState(BlockState newBlockState)
    {
        if (currentBlockState == newBlockState) //If the states are the same, there is no need to change.
        {
            return;
        }

        switch (newBlockState)
        {
            case BlockState.High:
                currentBlockState = BlockState.High;
                isBlocking = true;
                break;
            case BlockState.Low:
                currentBlockState = BlockState.Low;
                isBlocking = true;
                break;
            case BlockState.None:
                currentBlockState = BlockState.None;
                isBlocking = false;
                return;
        }
    }
}