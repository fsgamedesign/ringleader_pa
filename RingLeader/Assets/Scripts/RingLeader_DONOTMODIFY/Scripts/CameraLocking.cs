﻿//Date: 05/14/17
//Author: Timothy Cable (May, 17) / (continue adding authors as needed here.)
//Credits: Timothy Cable
//
//
//
//
//Purpose: To maintain the two ref cameras pointed always at their respective player.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLocking : MonoBehaviour
{
    public Transform target;

    void Update()
    {
        // Rotate the camera every frame so it keeps looking at the target
        transform.LookAt(target);
    }
}

