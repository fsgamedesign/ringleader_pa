﻿//Contributors: Tyler Fronczak

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punching : MonoBehaviour
{
    #region Instance Variables

    [Header("Stats")]
    public float punchSpeed = 10.0f;

    [Header("Input- Punch Type")]
    [SerializeField] KeyCode jab = KeyCode.H;
    [SerializeField] KeyCode cross = KeyCode.J;
    [SerializeField] KeyCode hook = KeyCode.K;
    [SerializeField] KeyCode uppercut = KeyCode.L;
    [SerializeField] KeyCode allIn = KeyCode.Space;

    [Header("Input- Punch Direction")]
    [SerializeField] KeyCode upperPunch = KeyCode.T;
    [SerializeField] KeyCode leftPunch = KeyCode.F;
    [SerializeField] KeyCode rightPunch = KeyCode.H;
    [SerializeField] KeyCode lowerPunch = KeyCode.G;

    [Header("References")]
    [SerializeField] GameObject leftGlove;
    [SerializeField] GameObject rightGlove;
    Vector3 leftGloveStartPos;
    Vector3 rightGloveStartPos;


    [SerializeField] Transform upperLeftHit;
    [SerializeField] Transform upperRightHit;
    [SerializeField] Transform leftHit;
    [SerializeField] Transform rightHit;
    [SerializeField] Transform lowerLeftHit;
    [SerializeField] Transform lowerRightHit;

    bool isLeftGloveReady = true;
    bool isRightGloveReady = true;

    PunchTypes punchType;
    enum PunchTypes
    {
        Jab,
        Cross,
        Hook,
        Uppercut,
        AllIn,
        None
    }

    PunchDirections punchDirection;
    enum PunchDirections
    {
        UpperLeft,
        UpperRight,
        Left,
        Right,
        LowerLeft,
        LowerRight,
        None
    }

    enum Gloves
    {
        Right,
        Left
    }
    #endregion

    void Awake()
    {
        leftGloveStartPos = leftGlove.transform.localPosition;
        rightGloveStartPos = rightGlove.transform.localPosition;
    }

    void Update()
    {
        punchType = DeterminePunchType();
        punchDirection = DeterminePunchDirection();

        if (punchType != PunchTypes.None)
        {
            if (punchType == PunchTypes.Jab)
            {
                Jab(punchDirection);
            }
            else if (punchType == PunchTypes.Cross)
            {
                Cross(punchDirection);
            }
            else if (punchType == PunchTypes.Hook)
            {
                Hook(punchDirection);
            }
            else if (punchType == PunchTypes.Uppercut)
            {
                Uppercut(punchDirection);
            }
            else if (punchType == PunchTypes.AllIn)
            {
                AllIn();
            }
        }
    }

    //Punch Types
    void Jab(PunchDirections direction)
    {
        if (direction == PunchDirections.UpperLeft || direction == PunchDirections.Left || direction == PunchDirections.LowerLeft)
        {
            if (isLeftGloveReady)
            {
                VisualizePunch(Gloves.Left, direction);
                Debug.Log(gameObject.name + " threw a/an " + direction.ToString() + " jab.");
            }
        }
        else if (direction == PunchDirections.UpperRight || direction == PunchDirections.Right || direction == PunchDirections.LowerRight)
        {
            if (isRightGloveReady)
            {
                VisualizePunch(Gloves.Right, direction);
                Debug.Log(gameObject.name + " threw a/an " + direction.ToString() + " jab.");
            }
        }
    }
    void Cross(PunchDirections direction)
    {
        if (direction == PunchDirections.UpperLeft || direction == PunchDirections.Left || direction == PunchDirections.LowerLeft)
        {
            if (isRightGloveReady)
            {
                VisualizePunch(Gloves.Right, direction);
                Debug.Log(gameObject.name + " threw a cross to the " + direction.ToString() + ".");
            }
        }
        else if (direction == PunchDirections.UpperRight || direction == PunchDirections.Right || direction == PunchDirections.LowerRight)
        {
            if (isLeftGloveReady)
            {
                VisualizePunch(Gloves.Left, direction);
                Debug.Log(gameObject.name + " threw a cross to the " + direction.ToString() + ".");
            }
        }
    }
    void Hook(PunchDirections direction)
    {
        if (direction == PunchDirections.UpperLeft || direction == PunchDirections.Left || direction == PunchDirections.LowerLeft)
        {
            if (isLeftGloveReady)
            {
                VisualizePunch(Gloves.Left, direction);
                Debug.Log(gameObject.name + " threw a/an " + direction.ToString() + " hook.");
            }
        }
        else if (direction == PunchDirections.UpperRight || direction == PunchDirections.Right || direction == PunchDirections.LowerRight)
        {
            if (isRightGloveReady)
            {
                VisualizePunch(Gloves.Right, direction);
                Debug.Log(gameObject.name + " threw a/an " + direction.ToString() + " hook.");
            }
        }
    }
    void Uppercut(PunchDirections direction)
    {
        if (direction == PunchDirections.UpperLeft || direction == PunchDirections.Left || direction == PunchDirections.LowerLeft)
        {
            if (isLeftGloveReady)
            {
                VisualizePunch(Gloves.Left, direction);
                Debug.Log(gameObject.name + " threw a/an " + direction.ToString() + " uppercut.");
            }
        }
        else if (direction == PunchDirections.UpperRight || direction == PunchDirections.Right || direction == PunchDirections.LowerRight)
        {
            if (isRightGloveReady)
            {
                VisualizePunch(Gloves.Right, direction);
                Debug.Log(gameObject.name + " threw a/an " + direction.ToString() + " uppercut.");
            }
        }
    }
    void AllIn()
    {
        Debug.Log(gameObject.name + " threw an all-in punch!!");
    }

    PunchTypes DeterminePunchType()
    {
        if (Input.GetKeyDown(jab))
        {
            return PunchTypes.Jab;
        }
        else if (Input.GetKeyDown(cross))
        {
            return PunchTypes.Cross;
        }
        else if (Input.GetKeyDown(hook))
        {
            return PunchTypes.Hook;
        }
        else if (Input.GetKeyDown(uppercut))
        {
            return PunchTypes.Uppercut;
        }
        else if (Input.GetKeyDown(allIn))
        {
            return PunchTypes.AllIn;
        }
        else
        {
            return PunchTypes.None;
        }
    }
    PunchDirections DeterminePunchDirection()
    {
        if (Input.GetKey(upperPunch) && Input.GetKey(leftPunch))
        {
            return PunchDirections.UpperLeft;
        }
        else if (Input.GetKey(upperPunch) && Input.GetKey(rightPunch))
        {
            return PunchDirections.UpperRight;
        }
        else if (Input.GetKey(lowerPunch) && Input.GetKey(leftPunch))
        {
            return PunchDirections.LowerLeft;
        }
        else if (Input.GetKey(lowerPunch) && Input.GetKey(rightPunch))
        {
            return PunchDirections.LowerRight;
        }
        else if (Input.GetKey(leftPunch))
        {
            return PunchDirections.Left;
        }
        else if (Input.GetKey(rightPunch))
        {
            return PunchDirections.Right;
        }
        else
        {
            return PunchDirections.None;
        }
    }

    GameObject GetReadyGlove()
    {
        if (isLeftGloveReady)
        {
            return leftGlove;
        }
        else if (isRightGloveReady)
        {
            return rightGlove;
        }
        else
        {
            return null;
        }
    }

    void VisualizePunch(Gloves glove, PunchDirections direction)
    {
        GameObject gloveObj;

        if (glove == Gloves.Left)
        {
            gloveObj = leftGlove;
            isLeftGloveReady = false;
        }
        else
        {
            gloveObj = rightGlove;
            isRightGloveReady = false;
        }

        switch (direction)
        {
            case PunchDirections.UpperLeft:
                gloveObj.transform.position = upperLeftHit.position;
                break;
            case PunchDirections.Left:
                gloveObj.transform.position = leftHit.position;
                break;
            case PunchDirections.LowerLeft:
                gloveObj.transform.position = lowerLeftHit.position;
                break;
            case PunchDirections.UpperRight:
                gloveObj.transform.position = upperRightHit.position;
                break;
            case PunchDirections.Right:
                gloveObj.transform.position = rightHit.position;
                break;
            case PunchDirections.LowerRight:
                gloveObj.transform.position = lowerRightHit.position;
                break;
        }

        StartCoroutine(ReturnGloveToStart(glove));
    }

    IEnumerator ReturnGloveToStart(Gloves glove)
    {
        yield return new WaitForSeconds(.7f);

        if (glove == Gloves.Left)
        {
            leftGlove.transform.localPosition = leftGloveStartPos;
            isLeftGloveReady = true;
        }
        else
        {
            rightGlove.transform.localPosition = rightGloveStartPos;
            isRightGloveReady = true;
        }
    }
}