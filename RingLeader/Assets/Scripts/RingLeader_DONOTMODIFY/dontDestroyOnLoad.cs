﻿/*------------------------------------------------------------------------------------------------
 Original Author: Eduardo Sarmiento
 Date: 7/26/2017

 Purpose:Don't destrot the object this is attached when the scene changes.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dontDestroyOnLoad : MonoBehaviour {

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
