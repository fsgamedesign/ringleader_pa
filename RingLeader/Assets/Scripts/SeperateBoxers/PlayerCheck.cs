﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Checking where the player is inside of the corner.
//                Start & End dates 06/13/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCheck : MonoBehaviour {
    [SerializeField]
    string boxerToCheckFor, boxerToCheckFor2;

    public bool boxer1Here, boxer2Here;

    void OnTriggerStay(Collider other)
    {
        if(other.name == boxerToCheckFor)
        {
            boxer1Here = true;
        }
        if(other.name == boxerToCheckFor2)
        {
            boxer2Here = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name == boxerToCheckFor)
        {
            boxer1Here = false;
        }
        if (other.name == boxerToCheckFor2)
        {
            boxer2Here = false;
        }
    }

}
