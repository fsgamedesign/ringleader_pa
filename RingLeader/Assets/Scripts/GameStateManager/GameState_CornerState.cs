﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_CornerState : GameState {

    //references/parameters/variables needed by this gamestate
    private bool ActionHappened = false;
    public bool cancelFight = false;
    private bool heal = false;

    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.
       

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {       
        currentStatus = StateStatus.Start;

        //TODO: anything this game state needs to do first when taking over as the current game state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {

        //Debug.Log("This is the current corner time :" + curCornerTime);
        
        /// Once the Current Corner Time is greater than or equal to 
        /// the MaxCornerTime, then transition to the Round Action Game State
        if (GameStateManager.gameStateManager.boutTimer.GetBreakClock() <= 0) {
            //Once the Current Corner Time is Greater than the Max Corner Time, transition to the Round Action State
            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.RoundAction);          
        }

        //TODO: anything this game state needs to do every frame.
        if (ActionHappened == true)
        {
            if (cancelFight == true)
            {
                CancelFight();
                ActionHappened = false;
            }
            if (heal == true)
            {
                Heal();
                ActionHappened = false;
            }
          
        }
    }

    public override void ExitState()
    {
        // Santiago Castaneda Munoz / Ring Leader August 
        // The CornerState should only be accessed if there are Round Remaining, therefore when exiting the 
        // CornerState the boxers should go back to fighting

        // Added bool to Start initiate the timer if it is a new round
        if (GameStateManager.gameStateManager.GetPreviousState() == Constants.GameState.RoundAction)
        {
            // Debugging for a New Round
            Debug.Log("<color=red>NEW ROUND</color>");

            // Activating the Round Timer
            GameStateManager.gameStateManager.boutTimer.GetinPlay = true;
        }
        
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
 
        currentStatus = StateStatus.Staged;

    }

    //functions/methods needed by this gamestate

    //Some of this fuctions don't have a functionallity yet.
    //I will explain what that functions needs to reference, and what to do once it hace the input.
    public void CancelFight()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //cancelFight = true;
        /* 
         * This function should look for the Cancel fight action, once tha action is
         * implemented and reference this action should stop the fight.
         */
    }
    public void Heal()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //heal = true;
        /* 
         * This function should look for the heal action, once tha action is
         * implemented and reference this action should reduce the KOP of the player.
         */
    }
}
