﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_BoutConditions : GameState {

    /// Santiago Castaneda Munoz / Ring Leader August:
    /// Adding variables to manage the number of Rounds 
    /// Int to Count the Max Number of Rounds
    private int maxRound;
    /// Mutator to Get or Set Max Number of Rounds
    public int SetMaxRound()
    {
        return maxRound;
    }

    //references/parameters/variables needed by this gamestate

    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        currentStatus = StateStatus.Start;
        // Load the Match Conditions Scene, which has the index of 3 in the build settings
        RingLeaderSceneManager.instance.ChangeScene(3);
        //TODO: anything this game state needs to do first when taking over as the current game state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
    }

    public override void ExitState()
    {

        // Santiago Castaneda Munoz / Ring Leader August Version: 
        // Setting the Value for the Number of Rounds Rounds. The player should be able to choose from a set of 
        // rounds according to the category, oponent,etc. Right now, each match consists of 3 rounds. 
        // The player should be able to choose different types of fights, 3,5,8,12 rounds...
        maxRound = 3;

        // Debugging for the Max Number of Rounds
        Debug.Log("<color=darkblue>THE MAX NUMBER OF ROUNDS HAS BEEN SET TO: </color>" + SetMaxRound());

        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
    public void StartFight()
    {
        /* 
         * When the player selects this option the game starts as a random character.
         */
        GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.RoundAction);
    }
}
