﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_RoundPause : GameState {

    private bool ActionHappened = false;
    private bool thrownTowel = false;
    private bool saveGame = false;
    private bool backToFight = false;

    //references/parameters/variables needed by this gamestate
    //We need to reference all GameObjects and scrips we may need.



    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.
     
        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        currentStatus = StateStatus.Start;

        //TODO: anything this game state needs to do first when taking over as the current game state.

        /// Santiago Castaneda/ Ring Leader August Version:
        /// When entering the Round Pause the timer should stop counting
        GameStateManager.gameStateManager.boutTimer.GetPaused = true;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
        //The update should look for input tha would change into another state.
        if (ActionHappened == true)
        {
            if (thrownTowel == true)
            {
                ThrownTowel();
                ActionHappened = false;
            }
            if (saveGame == true)
            {
                SaveGame();
                ActionHappened = false;
            }
            if (backToFight == true)
            {
                BackToFight();
                ActionHappened = false;
            }
          
        }
    }

    public override void ExitState()
    {
        /// Santiago Castaneda/ Ring Leader August Version:
        /// When exiting the Round Pause the timer should continue counting
        GameStateManager.gameStateManager.boutTimer.GetPaused = false;

        currentStatus = StateStatus.Exit;
        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
    public void ThrownTowel()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //thrownTowel = true;
        /* 
         * This function should look for the Thrown Towel action, once tha action is
         * implemented and reference this action should stop the round.
         */
    }
    public void SaveGame()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //  saveGame = true;
        /* 
       * This function should look for the Save Game input, the fight will be stoped until the player returns, 
       * coming back in the same round he saved.
       */
    }
    public void BackToFight()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //  backToFight = true;
        /* 
        * This function should look for the Back to the fight input,
        * the game retusn to the roundActionState
        */
    }

}
