﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: This script keeps track of things we don't want to keep typing over and over again.
 Everything in this namespace is globally accessible by adding using Constants to the namespace 
 list of a script. Do not abuse this namespace. Only put things in it that are globally relevant.
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Constants
{

    /*  Game State Comments
        https://trello.com/c/EmEzRU2e
        Menus = This state is active whenever the main menu is active.
                The Menus state is reserved for non bout setup/management related menus.

        BoxerSelection = This state is active on the boxer selection screen.
        
        RoundAction = When this state is active, the round clock is ticking. Boxers are fighting.

        CornerState = Is active when the boxers are in their respective corners. 
                      This is typically between rounds and different from the ref tells players to 
                      go to their corners during rounds.

        RoundPause = When this state is active the referee is making a command that stops the round timer,
                     or a round pausing menu is active.

        StandingCount = Whenever a boxer is knocked down and the referee is performing a standing 10 count.

        TKO = This state is active when a TKO has been declared.
        
        KO = This state is active when a KO has been declared. 
        
        JudgeCards = This state is active when a round has gone to decision,
                     or after a bout when judge cards are revealed.
        
        GameOver = This state is active when the players have exited the judge card screen and are 
                   viewing any final information plus the game over menu.
    */
    public enum GameState
    {
        Menus,
        BoxerSelection,
        BoutConditions,
        RoundAction,
        CornerState,
        RoundPause,
        StandingCount,
        TKO,
        KO,
        JudgeCards,
        GameOver,
    }

    public enum HitLocation
    {
        Head_Front,
        Head_Left,
        Head_Right,
        Head_Back,
        Chest_Left,
        Chest_Center,
        Chest_Right,
        Back,
        GutLeft,
        GutRight,
        BelowBelt
    }

    // Head, Chest, ect. are only the approximate punch locations,
    // assuming both characters are standing and facing eachother.
    public enum BoxerInput
    {
        Jab_Orthodox_Head,
        Jab_Orthodox_Chest,
        Jab_Orthodox_Gut,
        Jab_Southpaw_Head,
        Jab_Southpaw_Chest,
        Jab_Southpaw_Gut,

        Cross_Orthodox_Head,
        Cross_Orthodox_Chest,
        Cross_Orthodox_Gut,
        Cross_Orthodox_BelowBelt,
        Cross_Southpaw_Head,
        Cross_Southpaw_Chest,
        Cross_Southpaw_Gut,
        Cross_Southpaw_BelowBelt,

        Hook_Left_Head,
        Hook_Left_Chest,
        Hook_Left_Gut,
        Hook_Left_BelowBelt,
        Hook_Right_Head,
        Hook_Right_Chest,
        Hook_Right_Gut,
        Hook_Right_BelowBelt,

        Uppercut_Left_Head,
        Uppercut_Left_Chest,
        Uppercut_Left_Gut,
        Uppercut_Left_BelowBelt,
        Uppercut_Right_Head,
        Uppercut_Right_Chest,
        Uppercut_Right_Gut,
        Uppercut_Right_BelowBelt,

        Signature_Fair,
        Signature_Illegal,

        Push,
        Clinch,

        Block_High_Left,
        Block_High_Center,
        Block_High_Right,
        Block_Low_Left,
        Block_Low_Center,
        Block_Low_Right,

        Combo_Flurry,

        Dodge_Left,
        Dodge_Right,
        Dodge_Back
    }

    public enum RefInput
    {
        Break,
        Time,
        Box,
        Corner,
        Focus
    }
}