﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    void HandleMovement();
    void MovementAnimation(Vector3 _moveDirection); //Used to trigger animation controllers when movement is done.
}

public interface IPunching
{
    void HandlePunching();
}