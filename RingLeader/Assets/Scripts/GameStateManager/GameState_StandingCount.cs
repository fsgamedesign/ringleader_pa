﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/
// Date: 08/16/2017
// Author: Yi Li
// Purpose: Redo standing count functionality and connect to the system.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_StandingCount : GameState {

    //references/parameters/variables needed by this gamestate

    [SerializeField] float endTime;
    [SerializeField] float currentTime;
    private float countTime;
    private bool StartCount;

    public bool StartCount1
    {
        get
        {
            return StartCount;
        }

        set
        {
            StartCount = value;
        }
    }

    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        currentStatus = StateStatus.Start;
        //TODO: anything this game state needs to do first when taking over as the current game state.

        endTime = 10f;
        currentTime = 0f;
        countTime = 0f;
        StartCount = false;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
        Debug.Log("A player was knocked down, the referee can start standing count!");
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
        if (StartCount) {
            countTime += Time.deltaTime;
            currentTime = Mathf.FloorToInt(countTime); // Make the number in Inspector is a integer.
        }

        if (currentTime >= endTime)
        {
//<<<<<<< HEAD
            // Ring Leader July Version:
            //GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.Menus);

            // Santiago Castaneda/ Ring Leader August Version: Changing the Game Stater to KO 
            // if the player does not stand up after the standing count has been completed
            GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.KO);
//=======
            Debug.Log("Time's up! Let the referee declare the winner!");
            StartCount = false;
            currentTime = 10f;
            //countTime = 0f;
//>>>>>>> origin/LeiQian_RingLeader
        }
        

    }

    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
}
