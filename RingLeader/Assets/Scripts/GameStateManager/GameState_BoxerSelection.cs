﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_BoxerSelection : GameState {

    //references/parameters/variables needed by this gamestate
   


    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));
       

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.

        //This script should show the player selection UI and interface.

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        RingLeaderSceneManager.instance.ChangeScene(2);

        currentStatus = StateStatus.Start;
        //TODO: anything this game state needs to do first when taking over as the current game state.

        //This menu should be shown in this state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
        //This menu should look for input to initialize other states.
    }

    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
    public void HighlightBoxer()
    {
        /* 
         * When the player selects a boxer the boxer should be higlighthed, with a small description and stats been shown.
         */
        GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.BoutConditions);
    }

    // Santiago Castaneda Munoz: Adding Function to the Boxer Selection state to allow going back to the main menu
    public void ReturnToMenus()
    {
       /// Santiago Castaneda Munoz / Ring Leader August Version
       /// Changing the Game State to the Menus State
       GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.Menus);
    }
}
