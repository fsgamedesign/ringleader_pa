﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_Menus : GameState {

    //references/parameters/variables needed by this gamestate

    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

   
        //TODO: anything this game state needs to do on awake at the beginning of application runtime.

        //This script should show the menu UI and interface.

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0)
        {
            RingLeaderSceneManager.instance.ChangeScene(1);
        }

        /// Santiago Castaneda Munoz / Ring Leader August Version:
        /// Allowing the game to load the scene if the player goes back from the boxer selection
        if (GameStateManager.gameStateManager.GetPreviousState() == Constants.GameState.GameOver || GameStateManager.gameStateManager.GetPreviousState() == Constants.GameState.StandingCount || GameStateManager.gameStateManager.GetPreviousState() == Constants.GameState.BoxerSelection)
        {
            RingLeaderSceneManager.instance.ChangeScene(1);
        }

        currentStatus = StateStatus.Start;
        //TODO: anything this game state needs to do first when taking over as the current game state.

        //The Menu should be shown in this state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
        //The menu should look for input to initialize other states.
    }

    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
    public void RandomPlayerStart()
    {
        /* 
         * When the player selects this optio the game starts as a random character.
         */
        GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.BoxerSelection);

    }
    public void PasswordEnter()
    { 
        /* 
       * This option take the players to the password interface.
       */
    }
    public void PlayerSelect()
    {   
        /* 
        * This option gives the player the option to select what figther or referee he wants to play as.
        */
    }
    public void BackToMain()
    {    
        /* 
        * This option takes the player back to the main interface.
        */
    }
}