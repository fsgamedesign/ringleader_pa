﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_RoundAction : GameState {

    private bool ActionHappened = false;
    private bool thrownTowel = false;
    private bool disquialifyPlayer = false;
    private bool deductPoint = false;
    private bool pauseFight = false;

    // origin/LeiQian_RingLeader

    //references/parameters/variables needed by this gamestate
    /* This State is the In-Game state, the player shoud be allowed to move an fight,
       This State should look for input from the referee and the couch.
       If the State ends the player shouldn't be able to cotrol any longer.
    */
    //We need to reference all GameObjects and scrips we may need.

    // Santiago Castaneda Munoz / Ring Leader August:
    // Adding variables to manage the number of Rounds 
    // Once the current number of Rounds is greater than the
    // Max number of rounds the game should transition to the JudgeCards Game State
    // This can only happen if no Boxer has been KO.
    int curRound;

    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.
       
        //All movement should be disable when the players are in the state.
      
        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        // if(UnityEngine.SceneManagement.)
           
        currentStatus = StateStatus.Start;
        // Ring Leader July Version: Reloads the Scene every time the Round Action state is loaded
        // RingLeaderSceneManager.instance.ChangeScene(4);

        // Santiago Castaneda / Ring Leader August Version: Only loads the scene the first time the state is entered.
        // Allows to keep track of the different possible stats of the boxers during the fight.
        // if a boxer stands up he can continue fighthing 

        if (GameStateManager.gameStateManager.GetPreviousState() == Constants.GameState.BoutConditions)
        {
  
            // Setting the Number of Rounds to the passed number in the Game State BoutConditions          
            Debug.Log("<color=red>FIRST ROUND</color>");
            // Setting the Current Round to 1
            curRound = 1;
            // Increasing for the first round
            Debug.Log("<color=green>CURRENT ROUND</color> : " + curRound);
            // Load the Boxer Action Scene
            RingLeaderSceneManager.instance.ChangeScene(4);
            // Set the Bool to false so that this section can never be accessed again
        }

        // If New Round is true and the Previous Game state was the Corner State 
        // then Increase the Rounds Counter and Debug.Log its value
        else if (GameStateManager.gameStateManager.GetPreviousState() == Constants.GameState.CornerState)
        {
            // Increasing the Number of Rounds when Entering Round Action after the Corner Time has endeed
            curRound++;
            // Obtaining the Round Timer current time and setting it to 0
            GameStateManager.gameStateManager.boutTimer.RestMaxRoundClock();
            Debug.Log("<color=green>CURRENT ROUND</color> : " + curRound);
        }

        //TODO: anything this game state needs to do first when taking over as the current game state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {

        //TODO: anything this game state needs to do every frame.
        //The update should look for input tha would change into another state.
        if (ActionHappened == true)
        {
            if(thrownTowel == true)
            {
                ThrownTowel();
                ActionHappened = false;
            }
            if (disquialifyPlayer == true)
            {
                DisquialifyBoxer();
                ActionHappened = false;
            }
            if (deductPoint == true)
            {
                DeductPoint();
                ActionHappened = false;
            }
            if (pauseFight == true)
            {
                PauseFight();
                ActionHappened = false;
            }
        }

        // Checking if the Round Timer Exists
        if (GameStateManager.gameStateManager.boutTimer != null)
        {
            // If the Current Time in the Round timer is greater than the Max time in the Round timer
            if (GameStateManager.gameStateManager.boutTimer.GetRoundClock() <= 0)
            {
                // Deactivate the timer
                GameStateManager.gameStateManager.boutTimer.GetinPlay = false;

                /// Santiago Castaneda Munoz / Ring Leader August
                /// if The players are not in the last Round then send them to go to the Corner State
                if (curRound < GameStateManager.gameStateManager.boutConditionsGameState.SetMaxRound())
                {
                    /// Setting the Game State to the Corner State
                    GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.CornerState);
                }

                /// Once the current number of Rounds is greater than the MaxNumberofRounds
                /// the game should transition to the JudgeCards Game State
                /// This can only happen if no Boxer has been KO.
                else
                    /// Setting the Game State to the Judge Cards State
                    GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.JudgeCards);
            }
        }
    }
    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }
    //functions/methods needed by this gamestate

    //Some of this fuctions don't have a functionallity yet.
    //I will explain what that functions needs to reference, and what to do once it hace the input.

    public void ThrownTowel()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //thrownTowel = true;
    /* 
     * This function should look for the Thrown Towel action, once tha action is
     * implemented and reference this action should stop the round.
     */
}
    public void DisquialifyBoxer()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //  disquialifyPlayer = true;
        /* 
       * This function should look for the DisqualifyPlayer1 and the DisquialifyPlayer2 combos.
       * once implemented the game should change to the GameOverState.
       */
    }
    public void DeductPoint()
    {
        //this bool will be sent when the action is performed in its source to let the script know it happened.
        //  deductPoint = true;
        /* 
        * This function should look for the AddPointPlayer1 and AddPointPlayer2
        * Once implemented it should send a message to the PlayerStateMachine to deduct a point.
        */
    }
    public void PauseFight()
    {
        ///// Santiago Castaneda Munoz / Ring Leader August
        ///// Once the Referee calls to pause the fight the game should transition to the Round Pause State
        //if (pauseFight == true)
        //{
        //    /// Setting the Game State to the Round Pause State 
        //    GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.RoundPause);
        //}

        //this bool will be sent when the action is performed in its source to let the script know it happened.
        // pauseFight = true;
        /* 
        * This function should look for the StopFight combo.
        * Once implemented the game should go to the RoundPause State.
        */
    }

    public int GetCurrentRound() {
        return curRound;
    }
}
