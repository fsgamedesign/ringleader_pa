﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc
 Worked on by: Eduardo Sarmiento.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_JudgeCards : GameState {

    //references/parameters/variables needed by this gamestate


    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.
        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        currentStatus = StateStatus.Start;
               
        ///<summary>Santiago Castaneda Munoz / Ring Leader August Version: 
        ///At this point the game should transition to the Judge Cards Scene
        ///All the points, hits, ilegal punches, etc. Have to be taken in cosnsideration to decalre the winner
        /// Some type of message should appear on the Screen to stop the fight and then transition to the JudgeCards Scene. 

        ///RingLeaderSceneManager.instance.ChangeScene("Introduce the Index value for the JudgeCards Scene");

        //TODO: anything this game state needs to do first when taking over as the current game state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.

        /// Santiago Castaneda Munoz /Ring Leader August Version:
        /// Once a winner has been declared by the Judges, the game should load the Game Over State
        /// Since there is no functionality for the Judges so far the Game State Machine transitions 
        /// automatically to the GameOver State.
        /// THIS SHOULD GO ON THE EXIT STATE, BUT SINCE THERE IS NOTHING CALLING FOR THE EXIT I PLACED IT IN
        /// THE UPDATE
        GameStateManager.gameStateManager.ChangeGameState(Constants.GameState.GameOver);
    }

    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
}
