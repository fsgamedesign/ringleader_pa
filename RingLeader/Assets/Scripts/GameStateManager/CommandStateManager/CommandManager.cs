﻿//------------------------------------------------------------------------------------------------
// Author: Lei Qian
// Date: 8-14-2017
// Credit: N/A
// Purpose: Added Focus Mode Function

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandManager : MonoBehaviour
{
    public static CommandManager instance;
    public delegate void Seperate();
    public static event Seperate SeperateTheBoxers;

    public RefereeStateMachine refSM;
    public BoxerStateMachine1 redBoxSM;
    public BoxerStateMachine1 blueBOxSM;

    public GameObject Ref;//Lei Qian. Use for Focus Mode

    public GameObject Focus; //Lei Qian.Use for Focus Mode

    public GameObject FocusRed;//Lei Qian.Use for Focus Mode

    private bool isFinished = true;

    public float checkTimer;// cooldown timer for focus mode, you can check it in inspector.
    // Use this for initialization

    private void Awake()
    {
        instance = this;
    }

    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        checkTimer += Time.deltaTime;
	}

    IEnumerator SeparateCheck()
    {
        yield return new WaitForSeconds(0.25f);
        refSM.ChangeState<RefereeState_Idle>();
        redBoxSM.ChangeState<BoxerState_MovePunch1>();
        blueBOxSM.ChangeState<BoxerState_MovePunch1>();
        isFinished = true;
    }

    public bool Command_Separate()
    {

        if(isFinished == true)
        {
            if (SeperateTheBoxers != null)
            {
                isFinished = false;
                refSM.ChangeState<RefereeState_Command>();
                redBoxSM.ChangeState<BoxerState_Commanded>();
                blueBOxSM.ChangeState<BoxerState_Commanded>();
                SeperateTheBoxers();
                StartCoroutine(SeparateCheck());
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    //entering Focus Mode if it's not in cd
    public void Command_FocusView()
    {
        if(checkTimer > 10f) { 
        
        Debug.Log("<color=red>ENTERING THE FOCUS MODE!</color>");

        if (refSM.IsCurrentState<RefereeState_Focus>())
        {
            refSM.ChangeState<RefereeState_Paused>();
        }

        else
            refSM.ChangeState<RefereeState_Focus>();
        }
        else
        {
            Debug.Log("Focus Mode is still in CD!");
        }
    }

    public bool Command_GameOver()
    {
        if(isFinished)
        {
            refSM.ChangeState<RefereeState_BoutEnd>();
            //As of 7/28/17 there is no GameOver State for the boxer
            //if and when the Boxer recieves a GameOver State, set the boxer states to Game Over here
            //redBoxer.ChangeState<BoxerState_GameOver();
            //blueBoxer.ChangeState<BoxerState_GameOver(); 

            //Calling a method to change game state
            GameStateManager.gameStateManager.GameState_Over();
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool Command_KOStart(BoxerStateMachine1 boxerKD)
    {
        if (isFinished)
        {
            refSM.ChangeState<RefereeState_KOCount>();
            boxerKD.boxerData.enemyObj.GetComponent<BoxerStateMachine1>().ChangeState<BoxerState_Commanded>();

            //GameStateManager.gameStateManager.GameState_KOStart();

            return true;
        }
        else
        {
            return false;
        }
    }
    
    public void IsFinishedToggle(bool _isFinished)
    {
        isFinished = _isFinished;
    }

    //Focus mode cooldown timer
    public void ResetTimefocusview()
    {
        checkTimer = 0;
    }
}
