﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Disqualify player based on points
//                Start & End dates 06/13/2017
//                References:
//                        Links:
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisqualifyPlayer21 : Combo1 {

    PointsController points;

    private void Start()
    {
        points = FindObjectOfType<PointsController>();
    }

    public override void DoComboResult()
    {
        if(points.GetPlayer2Points() < 9)
        {
            Debug.Log("Player 2 has been disqualified.");
            //Call function to end round with Player 1 as winner.
        }
    }
    public override void Initilialize()
    {

    }
}
