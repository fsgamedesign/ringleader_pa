﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature - Combo system input to intialize standing counter
//                Start & End dates 06/06/2017
//                References:
//                        Links:
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingCountInitialization1 : Combo1 {

    StandingCountController scControl;
    StandingCountCancel scCancel;

    private void Start()
    {
        scControl = FindObjectOfType<StandingCountController>();
        scCancel = FindObjectOfType<StandingCountCancel>();
    }

    public override void DoComboResult()
    {
        scControl.CountControl(true);
        scCancel.enabled = true;
        enabled = false;
    }

    public override void Initilialize()
    {
    }
}
