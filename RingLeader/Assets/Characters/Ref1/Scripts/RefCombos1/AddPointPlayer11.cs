﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature: Point subtraction for fouls. Called in combo system by referee.
//                Start & End dates 06/01/2017
//                References:
//                        Links:
//*/


public class AddPointPlayer11 : Combo1 {

    PointsController points;

    SeperateBoxers separate;

    private void Start()
    {
        points = FindObjectOfType<PointsController>();
        separate = GetComponent<SeperateBoxers>();
    }

    // Use this for initialization
    public override void DoComboResult()
    {
        points.ModPlayer1Points(-1);
        //separate.DoComboResult();
    }

    public override void Initilialize()
    {

    }
}
