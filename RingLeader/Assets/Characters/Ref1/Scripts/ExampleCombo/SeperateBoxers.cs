﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-28-2017
// Credit: 

// Purpose: Referee Combo for separating the boxers


//------------------------------------------------------------------------------------------------
// Date: 08/14/2017
// Author: Yi Li
// Purpose: Connect this function to the system. Press Q and 1 to use in the game.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// must implement the abstract behaviors. Mouse over the class name if you are getting errors. Chances are you haven't 
// implementd the functions in the abstract class called combo.
public class SeperateBoxers : RefCombo
{
    public override void DoComboResult()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.RoundAction)
        {
            bool SeparateBegin = CommandManager.instance.Command_Separate();
            if (SeparateBegin)
                Debug.Log("<color=red>Separate was successful!</color>");
            else
                Debug.Log("Separate was not successful");
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }

    public override void Initilialize()
    {

    }
}
