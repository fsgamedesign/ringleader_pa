﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: Christian Cipriano 
//                Feature - Placeholder for stopping fights when the actual mechanism is in place
//                Start & End dates 06/13/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/
// Date: 08/14/2017
// Author: Yi Li
// Purpose: Connect this function to the system. Added warning points. 
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StopFight : RefCombo
{
    private PointsController points;

    public override void Start()
    {
        base.Start();
        points = GetComponent<PointsController>();
    }

    public override void DoComboResult()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.RoundAction)
        {
            //Call function/State to make the referee stop the fight based on fight conditions
            Debug.Log("Referee has stopped the fight");
            bool validCommand = CommandManager.instance.Command_GameOver();
            if (validCommand)
            {
                Debug.Log("<color=red>Fight was stopped!</color>");
                Debug.Log("Player 1 has " + points.GetPlayer1Points() + " points.");
                Debug.Log("Player 2 has " + points.GetPlayer2Points() + " points.");
                //Making boxers go to corners
                redBoxer.transform.position = redCorner;
                blueBoxer.transform.position = blueCorner;
            }
            else
            {
                Debug.Log("Fight was not stopped");
            }
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }
    public override void Initilialize()
    {

    }
}
