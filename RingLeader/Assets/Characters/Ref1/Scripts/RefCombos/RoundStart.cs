﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Placeholder for actual round start when rounds are formed
//                Start & End dates 06/13/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/
// Date: 08/14/2017
// Author: Yi Li
// Purpose: 
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundStart : RefCombo
{
    private GameState_RoundAction GRA;

    private bool StartRound = false;

    private int CameraWorkDone = 0;

    private CAH_CameraWorkStateMachine CameraWorkStateMachine;    

    public int GetCameraWorkDone
    {
        get
        {
            return CameraWorkDone;
        }

        set
        {
            CameraWorkDone = value;
        }
    }

    public override void Start()
    {
        base.Start();
        CameraWorkDone = 0;
        CameraWorkStateMachine = FindObjectOfType<CAH_CameraWorkStateMachine>();
        GRA = FindObjectOfType<GameState_RoundAction>();

    }
    public override void DoComboResult()
    {
        CameraWorkStateMachine.ChangeState<MainCameraState>();

        if (CameraWorkDone > 0)
        {
            //Call function to begin round
            if (!StartRound)
            {
                GameStateManager.gameStateManager.boutTimer.GetPaused = false;
                GameStateManager.gameStateManager.BoxerInput.GetComponent<BoxerStateMachine1>().ChangeState<BoxerState_MovePunch1>();
                GameStateManager.gameStateManager.BoxerInput_2.GetComponent<BoxerStateMachine1>().ChangeState<BoxerState_MovePunch1>();                
                Debug.Log("The referee started the round!");
                StartRound = true;
            }
      
        }
 
        //Set check to ensure player cannot start round multiple times in a single round
    }
    public override void Initilialize()
    {
        
    }
}
