﻿// Date: 08/16/2017
// Author: Yi Li
// Purpose: The method to call Focus View functionality. Press C and 2 to use this command.
// 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusView : RefCombo
{

    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    public override void DoComboResult()
    {

        CommandManager.instance.Command_FocusView();
        Debug.Log("<color=red>The referee used the Focus View!</color>");
    }

    public override void Initilialize()
    {

    }
}
