﻿// Date: 08/12/2017
// Author: Yi Li
// Purpose: Based on the original script, added Referee's commands. 
using System.Collections.Generic;
using UnityEngine;

public class RefCombo : Combo1
{
    public GameObject redBoxer, blueBoxer;

    public Vector3 redCorner, blueCorner;

    public RefereeStateMachine Rsm;

    public virtual void Start()
    {
        Rsm = FindObjectOfType<RefereeStateMachine>();
        redBoxer = GameObject.Find("Boxer_Red");
        blueBoxer = GameObject.Find("Boxer_Blue");

        redCorner = new Vector3(-6f, 0f, -6f);
        blueCorner = new Vector3(6f, 0f, 6f);
    }

    public override void DoComboResult()
    {
        Debug.Log("Performing Combo!");
    }

    public override void Initilialize()
    {

    }
}
