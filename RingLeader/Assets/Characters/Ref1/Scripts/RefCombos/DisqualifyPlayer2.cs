﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: Christian Cipriano - provided functionality
//                Feature - Disqualify player based on points
//                Start & End dates 06/13/2017
//                References:
//                        Links:
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisqualifyPlayer2 : RefCombo
{

    PointsController points;
    bool finish = false;

    public override void Start()
    {
        base.Start();
        points = FindObjectOfType<PointsController>();
    }

    public override void DoComboResult()
    {
            Debug.Log("Player 2 has been disqualified.");
            //Call function to end round with Player 2 as winner.
            bool wasCommandValid = CommandManager.instance.Command_GameOver();
            if (wasCommandValid)
            {
                Debug.Log("<color=red>DisqualifyP2 was successful!</color>");
                redBoxer.transform.position = new Vector3(0f, 0f, 0f);
                blueBoxer.transform.position = blueCorner;
                finish = true;
            }
            else
            {
                Debug.Log("DisqualifyP2 was not successful");
            }
    }
    public override void Initilialize()
    {

    }
}
