﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature - Combo system input to stop standing count
//                Start & End dates 06/06/2017
//                References:
//                        Links:
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingCountCancel : RefCombo {

    StandingCountController scControl;
    StandingCountInitialization scInitialize;
    GameState_StandingCount GSC;

	// Use this for initialization
	public override void Start () {
        base.Start();
        scControl = FindObjectOfType<StandingCountController>();
        scInitialize = FindObjectOfType<StandingCountInitialization>();
        GSC = FindObjectOfType<GameState_StandingCount>();
    }
	
    public override void DoComboResult()
    {
        // Pause the counting.
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.StandingCount)
        {
            Debug.Log("The Referee paused standing count!");
            GSC.StartCount1 = false;
            scInitialize.enabled = true;
            enabled = false;
        }
    }
    public override void Initilialize()
    {
     
    }
}
