﻿// Date: 08/16/2017
// Author: Yi Li
// Purpose: Use for the referee to declare the fight condition. Press C and 3 to use this command.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeclaringFightCondition : RefCombo {

    [SerializeField] GameObject text;
    private GameObject textclone;
    private float open2 = 0;

	// Use this for initialization
	public override void Start () {

	}

    public override void DoComboResult()
    {
        open2++;
        if (open2 == 1)
        {
            Debug.Log("<color=red>Fight Conditions: </color>");
            Debug.Log("<color=red>1.Time limit is reached, 1 minute, before one of the boxers is knocked down.</color>");
            Debug.Log("<color=red>2.If a boxer is knocked down before the time limit is reached, that boxer loses.</color>");
            textclone = Instantiate(text, new Vector3(-9.47f, 10.5f, -0.91f), Quaternion.Euler(65, 0, 0));
        }
        if (open2 == 2)
        {
            Destroy(textclone);
            open2 = 0;
        }

    }

    public override void Initilialize()
    {
       
    }
}
