﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: Christian Cipriano
//                Feature - Combo system input to intialize standing counter
//                Start & End dates 06/06/2017
//                References:
//                        Links:
//*/
// Date: 08/16/2017
// Author: Yi Li
// Purpose: Redo Standingcount funcitionality and connect to the system.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingCountInitialization : RefCombo
{
    private StandingCountController scControl;
    private StandingCountCancel scCancel;
    private GameState_StandingCount GSC;
    private int okStart = 0;

    public override void Start()
    {
        base.Start();
        scControl = FindObjectOfType<StandingCountController>();
        scCancel = FindObjectOfType<StandingCountCancel>();
        GSC = FindObjectOfType<GameState_StandingCount>();
    }

    public override void DoComboResult()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.StandingCount)
        {
            okStart++;
            // Only in StandingCount Game State, the referee can make this command.
            if (okStart == 1)
            {
                Debug.Log("<color=red>The Referee started standing count!</color>");
                GSC.StartCount1 = true;
            }
            else if (okStart == 2)
            {
                Debug.Log("<color=red>The Referee paused standing count!</color>");
                GSC.StartCount1 = false;
                okStart = 0;
            }
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }

    public override void Initilialize()
    {
    }
}
