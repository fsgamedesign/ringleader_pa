﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Disqualify player based on points
//                Start & End dates 06/13/2017
//                References:
//                        Links:
//*/
// Date: 08/14/2017
// Author: Yi Li
// Purpose: Connect the function to the system. Press Q and 5 to use in the game.
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The combo should not have direct functionality in it
//The functionality should be called from the CommandManager
public class DisqualifyPlayer1 : RefCombo
{
    private PointsController points;
    private bool finish = false;

    public override void Start()
    {
        base.Start();
        points = FindObjectOfType<PointsController>();
    }

    public override void DoComboResult()
    {
            Debug.Log("Player 1 has been disqualified.");
            //Call function to end round with Player 2 as winner.
            bool wasCommandValid = CommandManager.instance.Command_GameOver();
            if(wasCommandValid)
            {
                Debug.Log("<color=red>DisqualifyP1 was successful!</color>");
                redBoxer.transform.position = redCorner;
                blueBoxer.transform.position = new Vector3(0f, 0f, 0f);
                finish = true;
            }
            else
            {
                Debug.Log("DisqualifyP1 was not successful!");
            }
            //Move winner to middle
    }
    public override void Initilialize()
    {

    }
}
