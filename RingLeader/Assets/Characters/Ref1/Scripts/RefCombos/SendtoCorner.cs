﻿// Date: 08/16/2017
// Author: Yi Li
// Purpose: Send the players to corners. Press C and 1 on the keyboard to use this command.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendtoCorner : RefCombo {

    private BoxerStateMachine1 red;
    private BoxerStateMachine1 blue;

	// Use this for initialization
	public override void Start () {
        base.Start();
        red = redBoxer.GetComponent<BoxerStateMachine1>();
        blue = blueBoxer.GetComponent<BoxerStateMachine1>();
	}

    public override void DoComboResult()
    {
        // Send both players to the corners during RoundAction State.
      if(GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.RoundAction)
        {
            redBoxer.transform.position = redCorner;
            blueBoxer.transform.position = blueCorner;
            Debug.Log("<color=red>Sent the players to the corners!</color>");
        }
      // Send the player who still stands up to the corner during StandingCount State.
      if(GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.StandingCount)
        {
            if (red.IsCurrentState<BoxerState_KnockedDown>())
            {
                blueBoxer.transform.position = blueCorner;
                Debug.Log("<color=red>Sent the blue player to the corner!</color>");
            }
            else if (blue.IsCurrentState<BoxerState_KnockedDown>())
            {
                redBoxer.transform.position = redCorner;
                Debug.Log("<color=red>Sent the red player to the corner!</color>");
            }
        }
    }

    public override void Initilialize()
    {

    }
}
