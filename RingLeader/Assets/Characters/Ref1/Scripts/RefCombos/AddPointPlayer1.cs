﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature: Point subtraction for fouls. Called in combo system by referee.
//                Start & End dates 06/01/2017
//                References:
//                        Links:
//*/
// Date: 08/11/2017
// Author: Yi Li
// Purpose: Combine this function to the system. Press Q and 2 to use in the game.
using UnityEngine;
public class AddPointPlayer1 : Combo1 {

    PointsController points;

    SeperateBoxers separate;

    private void Start()
    {
        points = FindObjectOfType<PointsController>();
        separate = GetComponent<SeperateBoxers>();
    }

    // Use this for initialization
    public override void DoComboResult()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.RoundAction || GameStateManager.gameStateManager.GetCurrentGameState() == Constants.GameState.CornerState)
        {
            points.ModPlayer1Points(-1);
            Debug.Log("<color=red>Player1 points -1. Current points are </color>" + points.GetPlayer1Points() + ".");
        }
        else
        {
            Debug.Log("You cannot execute this command in this Game State!");
        }
    }

    public override void Initilialize()
    {

    }
}
