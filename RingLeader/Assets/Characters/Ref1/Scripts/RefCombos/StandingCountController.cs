﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature - Data model in order to control the standing count.
//                Start & End dates 06/06/2017
//                References:
//                        Links:
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingCountController : MonoBehaviour {

    float count = 0;
    public int curCount;

    public bool isCounting = false;


	// Use this for initialization
	void Start () {
        curCount = 0; //Count will always start at 0
	}
	
	// Update is called once per frame
	void Update () {
        if (isCounting)
        {
            count += Time.deltaTime;
            curCount = Mathf.FloorToInt(count); //converting count to an integer to only show whole numbers in inspector
            //Debug.Log("Current count " + curCount);
            if(count >= 10.0f)
            {
                count = 10.0f;
            }
        }
        else
        {
            count = 0;
        }
	}
    public void CountControl(bool _status)
    {
        isCounting = _status;
    }
}
