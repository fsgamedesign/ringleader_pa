﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 6/15/2017
// Cody Sneed
// Help and collaberation with Jacob May
// Changed all public gameObjects into private transforms because that is what we are using them for. 
// This also allows them to be declared in the start() and wont have to manually drag in the gameObjects everytime you use the prefab.
// Made changes to reduce the warnings of the script for lack of declaring variables.
// Made a boost/turbo for the referee with the T keycode.
// Made an animation with the alpha 7 keycode.
// Made the ref be able to target onto specifc objects with key presses.
// Basic referee movement, crouching and locking onto players/objects in the ring.
//
[RequireComponent(typeof(CharacterController))]
public class Ref_Movement : MonoBehaviour
{
    #region Instance Variables
    [Header("Stats")]
    [SerializeField]
    private float moveSpeed; // defined in the inspector on the New_Ref prefab.

    [SerializeField]
    private float rotationSpeed = 0;

    [Header("Input")]
    [SerializeField]
    private string horizontalAxis = ""; // movement is similar to the red boxer
    [SerializeField]
    private string verticalAxis = "";
    // horizontal movement is with W (moving forward) and S (moving backwards)
    // speed boost is alpha 7 (when holding the button down)
    [SerializeField]
    private KeyCode leftInput = KeyCode.A; // is move to the left or rotate to the left (changes depending on if in the locked or free state)
    [SerializeField]
    private KeyCode rightInput = KeyCode.D; // is move to the right or rotate to the right (changes depending on if in the locked or free state)
    [SerializeField]
    private KeyCode toggleMoveMode = KeyCode.C;
    [SerializeField]
    private KeyCode toggleMoveModeRed = KeyCode.Alpha0;
    [SerializeField]
    private KeyCode toggleMoveModeBlue = KeyCode.Alpha9;
    [SerializeField]
    private KeyCode toggleMoveModeNuetral = KeyCode.Alpha8;


    //objects the referee locks onto (using the inspector)
    [SerializeField]
    private Transform lock_centerObj;
    [SerializeField]
    private Transform lock_Red;
    [SerializeField]
    private Transform lock_RedCorner;
    [SerializeField]
    private Transform lock_Blue;
    [SerializeField]
    private Transform lock_BlueCorner;
    [SerializeField]
    private Transform lock_NuetralCorner1;
    [SerializeField]
    private Transform lock_NuetralCorner2;
    [SerializeField]
    private Transform lock_Judge;

    RotationDirection rotationDirection;
    enum RotationDirection
    {
        Left,
        Right,
        None
    }

    MoveMode moveMode = MoveMode.Locked;
    enum MoveMode
    {
        Free,
        Locked, //input c  (this is for centerpoint and cycles between locked and free)
        Locked_Red,       // input alpha 0 and cycles through Locked_Red, Locked_RedCorner and Locked
        Locked_RedCorner,
        Locked_Blue,       //input alpha 9 and cycles through through Locked_Blue, Locked_BlueCorner and Locked
        Locked_BlueCorner,
        Locked_NeutralCorner1, // input alpha 8 and cycles through through Locked_NeutralCorner1, Locked_NeutralCorner2, Judge and Locked
        Locked_NeutralCorner2,
        Locked_Judge,
        None
    }

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    private Animator anim;
    private bool forAnimBoolChange = false;
    #endregion

    void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Start() // sets up each transform and anim
    {
        anim = GetComponent<Animator>(); //animation current place holder
        lock_Red = lock_Red.GetComponent<Transform>();
        lock_RedCorner = lock_RedCorner.GetComponent<Transform>();
        lock_Blue = lock_Blue.GetComponent<Transform>();
        lock_BlueCorner = lock_BlueCorner.GetComponent<Transform>();
        lock_NuetralCorner1 = lock_NuetralCorner1.GetComponent<Transform>();
        lock_NuetralCorner2 = lock_NuetralCorner2.GetComponent<Transform>();
        lock_Judge = lock_Judge.GetComponent<Transform>();
        lock_centerObj = lock_centerObj.GetComponent<Transform>();
    }

    void Update()
    {
       // Debug.Log(moveMode + " is current state");
        HandleInput();
    }

    void ToggleMoveMode()
    {
        switch (moveMode)
        {
            case MoveMode.Free:
                moveMode = MoveMode.Locked;
                break;
            case MoveMode.Locked:
                moveMode = MoveMode.Free;
                break;
  //the cases below just make it so that the player can switch back to free mode in case they need to change directions faster.
            case MoveMode.Locked_Red:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.Locked_RedCorner:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.Locked_Blue:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.Locked_BlueCorner:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.Locked_NeutralCorner1:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.Locked_NeutralCorner2:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.Locked_Judge:
                moveMode = MoveMode.Free;
                break;
            case MoveMode.None:
                return;

        } // end of switch function
    } // end of ToggleMoveMode()

    void ToggleMoveModeRed()
    // Cycles between boxer red, red corner and centerpoint. First two cases are needed in order to 
    // switch from Free or Locked enum into the Locked_Red enum/case.
    {
        switch (moveMode)
        {
            case MoveMode.Free:
                moveMode = MoveMode.Locked_Red;
                break;
            case MoveMode.Locked:
                moveMode = MoveMode.Locked_Red;
                break;
            case MoveMode.Locked_Red:
                moveMode = MoveMode.Locked_RedCorner;
                break;
            case MoveMode.Locked_RedCorner:
                moveMode = MoveMode.Locked;
                break;
            case MoveMode.None:
                return;
        } // end of switch function
    } // end of ToggleMoveModeRed()

    void ToggleMoveModeBlue()
    // Cycles between boxer blue, blue corner and centerpoint. First two cases are needed in order to 
    // switch from Free or Locked enum into the Locked_Blue enum/case.
    {
        switch (moveMode)
        {
            case MoveMode.Free:
                moveMode = MoveMode.Locked_Blue;
                break;
            case MoveMode.Locked:
                moveMode = MoveMode.Locked_Blue;
                break;
            case MoveMode.Locked_Blue:
                moveMode = MoveMode.Locked_BlueCorner;
                break;
            case MoveMode.Locked_BlueCorner:
                moveMode = MoveMode.Locked;
                break;
            case MoveMode.None:
                return;
        } // end of switch function
    } // end of ToggleMoveModeBlue()

    void ToggleMoveModeNeutral()
    // Cycles between nuetral corner 1, nuetral corner 2, Judge and centerpoint. First two cases are needed in order to 
    // switch from Free or Locked enum/case into the Locked_NeutralCorner1 enum/case.

    {
        switch (moveMode)
        {
            case MoveMode.Free:
                moveMode = MoveMode.Locked_NeutralCorner1;
                break;
            case MoveMode.Locked:
                moveMode = MoveMode.Locked_NeutralCorner1;
                break;
            case MoveMode.Locked_NeutralCorner1:
                moveMode = MoveMode.Locked_NeutralCorner2;
                break;
            case MoveMode.Locked_NeutralCorner2:
                moveMode = MoveMode.Locked_Judge;
                break;
            case MoveMode.Locked_Judge:
                moveMode = MoveMode.Locked;
                break;
            case MoveMode.None:
                return;
        } // end of switch function
    } // end of toggleMoveModeNeutral()

    void FixedUpdate()
    // updates which ever case the referee is in.
    {
        switch (moveMode)
        {
            case MoveMode.Free:
                HandleFreeMovement();
                HandleFreeRotation(rotationDirection);
                break;
            case MoveMode.Locked:
                transform.LookAt(lock_centerObj);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_Red:
                transform.LookAt(lock_Red);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_RedCorner:
                transform.LookAt(lock_RedCorner);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_Blue:
                transform.LookAt(lock_Blue);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_BlueCorner:
                transform.LookAt(lock_BlueCorner);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_NeutralCorner1:
                transform.LookAt(lock_NuetralCorner1);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_NeutralCorner2:
                transform.LookAt(lock_NuetralCorner2);
                HandleLockedMovement();
                break;
            case MoveMode.Locked_Judge:
                transform.LookAt(lock_Judge);
                HandleLockedMovement();
                break;
            case MoveMode.None:
                return;
        } // end of switch function
    } // end of FixedUpdate()

    void HandleInput()
    {

        if (Input.GetKey(leftInput))
        {
            rotationDirection = RotationDirection.Left;
        }
        else if (Input.GetKey(rightInput))
        {
            rotationDirection = RotationDirection.Right;
        }
        else
        {
            rotationDirection = RotationDirection.None;
        }
        if (Input.GetKeyDown(toggleMoveMode)) //toggles between locked onto center and free mode. These Toggle functions just cycle through each of the cases
        {
            ToggleMoveMode();
        }
        if (Input.GetKeyDown(toggleMoveModeRed))
        {
            ToggleMoveModeRed();
        }
        if (Input.GetKeyDown(toggleMoveModeBlue))
        {
            ToggleMoveModeBlue();
        }
        if (Input.GetKeyDown(toggleMoveModeNuetral))
        {

            ToggleMoveModeNeutral();
        }
        if (Input.GetKeyDown(KeyCode.T)) //speed boost
        {
            moveSpeed += 10;
        }
        if (Input.GetKeyUp(KeyCode.T)) //sets speed back to normal
        {
            moveSpeed -= 10;
        }
        if (Input.GetKeyDown(KeyCode.Alpha7) && !forAnimBoolChange)
        { anim.SetBool("isDodging", true); forAnimBoolChange = true; }

        else if (Input.GetKeyDown(KeyCode.Alpha7) && forAnimBoolChange)
        { anim.SetBool("isDodging", false); forAnimBoolChange = false; }

    } // end of HandleInput()

    void HandleFreeMovement()
    {
        moveDirection = new Vector3(0, 0, Input.GetAxis(verticalAxis));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, 0, transform.position.z); //Keeps the boxer's feet on the boxing canvas.
    } //End of HandleFreMovement()

    void HandleLockedMovement()
    {
        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, 0, transform.position.z); //Keeps the boxer's feet on the boxing canvas.
    } // end of HandleLockedMovement()

    void HandleFreeRotation(RotationDirection direction)
    {
        switch (direction)
        {
            case RotationDirection.Left:
                transform.Rotate(0.0f, -1 * rotationSpeed, 0.0f);
                break;
            case RotationDirection.Right:
                transform.Rotate(0.0f, 1 * rotationSpeed, 0.0f);
                break;
            case RotationDirection.None:
                return;
        } // end of switch statement

    } // end of HandleFreeRotation()

} // end of public class
