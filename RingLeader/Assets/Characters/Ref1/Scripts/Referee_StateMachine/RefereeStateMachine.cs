﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-26-2017
// Credit: This script is heavily modeled after the MachineBehaviour and the BoxerStateMachine

// Purpose: State Machine for the Referee





using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController))]

public class RefereeStateMachine : ByTheTale.StateMachine.MachineBehaviour
{
    [HideInInspector]
    public Animator refAnimator;
    [HideInInspector]
    public CharacterController refController;

    public GameStateManager gSM;

    public bool paused;

    public bool checking = false; //Focus Mode checking if in the focus mode

    public override void AddStates()
    {
        //Add all the referee states. Well to add them I would need to make them first. 
        //There are no official referee states just yet, so these will have to do for now.
        AddState<RefereeState_BoutStart>();
        AddState<RefereeState_BoutEnd>();
        AddState<RefereeState_Idle>();
        AddState<RefereeState_Command>();
        AddState<RefereeState_Focus>();
        AddState<RefereeState_KOCount>();
        AddState<RefereeState_Paused>();
        AddState<RefereeState_InBetween>();

        //After adding set the first state of the referee as the default state
        SetInitialState<RefereeState_Paused>();
    }

    public void Awake()
    {
        refAnimator = GetComponent<Animator>();
        refController = GetComponent<CharacterController>();
        gSM = FindObjectOfType<GameStateManager>();
    }

    public override void Initialize()
    {
        AddStates();
        currentState = initialState; //Initial state is paused
    }

    //Pause function. Sets the state to pause, stores previous state, and reverts to previous state when unpaused.
    ByTheTale.StateMachine.State prevState = null;
    public void Pause()
    {
        paused = !paused;

        if (paused)
        {
            prevState = currentState;
            ChangeState<RefereeState_Paused>();
        }
        else
        {
            ChangeState(prevState.GetType());
            Debug.Log(currentState);
        }
    }
    public void StateChange(Constants.GameState newState)
    {
        switch (newState)
        {
            case Constants.GameState.RoundAction:
                ChangeState<RefereeState_Command>();
                break;
            case Constants.GameState.CornerState:
                ChangeState<RefereeState_InBetween>();
                break;
            case Constants.GameState.StandingCount:
                ChangeState<RefereeState_KOCount>();
                break;
            case Constants.GameState.RoundPause:
                ChangeState<RefereeState_Paused>();
                break;
            case Constants.GameState.JudgeCards:
                ChangeState<RefereeState_BoutEnd>();
                break;
        }
    }
    //Focus mode statechange for refstate
    public void RefStateChange(Constants.RefInput newState)
    {
        switch (newState)
        {
            case Constants.RefInput.Focus:
                ChangeState<RefereeState_Focus>();
                break;
        }

    }

    private void OnEnable()
    {
        GameStateManager.GameStateHasChanged += StateChange;
    }

    private void OnDisable()
    {
        GameStateManager.GameStateHasChanged -= StateChange;
    }


}
