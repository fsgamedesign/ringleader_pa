﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-18-2017
// Credit: 

// Purpose: Script for the referee state of InBetweenRounds - the state of the referee before the fight start

//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_InBetween : RefereeState
{
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        //gSM.ChangeGameState(Constants.GameState.CornerState);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
