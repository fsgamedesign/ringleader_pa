﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-18-2017
// Credit: 

// Purpose: Script for the referee state of KO Count - The state the referee is in when a boxer is down

//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_KOCount : RefereeState
{
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        gSM.ChangeGameState(Constants.GameState.StandingCount);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
