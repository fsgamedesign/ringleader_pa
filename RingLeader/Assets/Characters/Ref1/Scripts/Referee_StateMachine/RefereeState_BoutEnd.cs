﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-18-2017
// Credit: 

// Purpose: Script for the referee state of Bout End - the state of the referee after the fight ends

//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_BoutEnd : RefereeState
{

    public override void Enter()
    {
        base.Enter();
        //gSM.ChangeGameState(Constants.GameState.GameOver);
    }
}
