﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState : ByTheTale.StateMachine.State
{
    public RefereeStateMachine fSM { get { return (RefereeStateMachine)machine; } }
    public GameStateManager gSM;
    public CommandManager cMN;

    // Use this for initialization
    public override void Enter()
    {
        gSM = GameObject.FindObjectOfType<GameStateManager>();
        cMN = GameObject.FindObjectOfType<CommandManager>();
    }

    // Update is called once per frame
    public override void Execute()
    {
        base.Execute();
    }
}
