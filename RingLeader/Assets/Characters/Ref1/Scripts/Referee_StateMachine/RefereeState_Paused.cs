﻿//------------------------------------------------------------------------------------------------

// Author: Christian Cipriano
// Date: 7-12-2017
// Credit: 

// Purpose: Command State for the Referee

//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_Paused : RefereeState
{
    public override void Enter()
    {
        base.Enter();
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
