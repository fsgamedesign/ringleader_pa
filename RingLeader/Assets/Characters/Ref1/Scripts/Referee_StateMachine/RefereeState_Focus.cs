﻿//------------------------------------------------------------------------------------------------

// Author: Lei Qian
// Date: 8-13-2017
// Credit: 

// Purpose: Script for the referee state of Focus - Special Referee action

//------------------------------------------------------------------------------------------------


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefereeState_Focus : RefereeState
{

    public float distancePerFrame;

   
    //public bool checking = false;

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("check if im in focus mode");
        cMN.ResetTimefocusview();
        cMN.Focus.SetActive(true);

        GetMachine<RefereeStateMachine>().checking = true;
               
    }

    public override void Execute()
    {
        base.Execute();
        if (Input.GetKeyDown(KeyCode.B))//Press B to switch to focus on Red Boxer
        {
            cMN.Focus.SetActive(false);
            cMN.FocusRed.SetActive(true);
        }

        if (cMN.checkTimer > 5)
        {
            machine.ChangeState<RefereeState_Paused>();

        }

    }

    public override void Exit()
    {
        base.Exit();

        GetMachine<RefereeStateMachine>().checking = false;
       cMN.Focus.SetActive(false);
       cMN.FocusRed.SetActive(false);
    }

}

