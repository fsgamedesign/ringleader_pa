﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Smash through the enemy'd defence/ KOP with consecutive upper cuts
//                        Add a custom animation for last hit
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: Left Hook, Right Hook, Left Hook
//        Dates 07/19/2017
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class Unrelenting : Combo1
{


	void Start ()
    {
		
	}
	

	void Update ()
    {
		
	}

    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Unrelenting Success");
    }

    public override void Initilialize()
    {
      
    }
}
