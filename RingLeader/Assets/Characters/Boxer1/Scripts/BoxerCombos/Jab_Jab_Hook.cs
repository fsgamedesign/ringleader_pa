﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan For Combo: A Simple combo performed by jabing twice to open the gard and a hook as a finishing blow
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: Jab Right, Jab Right, Hook, Jab Right
//        Dates 07/12/2017
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class Jab_Jab_Hook : Combo1
{

	
	void Start ()
    {
		
	}
	
	
	void Update ()
    {
		
	}

    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Jab, Jab, Hook Success");
    }

    public override void Initilialize()
    {
     
    }
}
