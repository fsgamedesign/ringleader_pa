﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Perform a simple starter combo action
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: left jab, Analog down, Right upper cut
//        Dates 07/26/2017
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class LeanBack : Combo1
{


    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Lean Back Success");
    }

    public override void Initilialize()
    {

    }
}