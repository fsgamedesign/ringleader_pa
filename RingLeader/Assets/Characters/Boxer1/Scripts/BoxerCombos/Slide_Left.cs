﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Stike with the left hook and shift over left similtaniously
//                        Move the player left.
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: W, F, R
//        Dates 07/16/2017
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class Slide_Left : Combo1 {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Slide Left Success");
    }

    public override void Initilialize()
    {
       
    }
}
