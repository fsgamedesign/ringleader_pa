﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Perform a combo that involves the boxer switching between right and left hooks. 
//                 Increase the kop damage and increase stamina drain on block.
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: H, G, T
//        Dates 07/16/2017
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class Illegal_Procedure: Combo1
{


    void Start()
    {

    }


    void Update()
    {

    }

    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Illegal Procedure Success");
    }

    public override void Initilialize()
    {
   
    }
}