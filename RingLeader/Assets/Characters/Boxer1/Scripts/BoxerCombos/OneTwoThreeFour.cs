﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Similar to the jab, jab, hook this combo ends with an upper cut for a greater risk reward
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: Left Jab, Right Jab, Hook, Left Jab, Uppercut, Right Jab
//        Dates 06/27/2017
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// To make additional Combos the class must inherent form the Combo1 script
public class OneTwoThreeFour : Combo1
{
	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
	{
		Debug.Log ("1, 2, 3, 4 punch");
	}

	public override void Initilialize()
	{

	}
}
