﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Perform a simple starter combo action
               
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: Left Jab, Hook, Right Jab
//        Dates 07/12/2017
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// To make additional Combos the class must inherent form the Combo1 script
public class Jab_RightHook : Combo1
{


    void Start()
    {

    }


    void Update()
    {

    }

    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Jab, Right Hook Success");
    }

    public override void Initilialize()
    {
        
    }
}