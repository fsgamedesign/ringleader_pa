﻿/*        
//        Developer Name: Mitchell Ford
//        Contribution: Boxer Combos
//        Plan for Combo: Jab the enemy and move up and to the right
                
                           Standing combos can be hooked up to the boxer, but at this time the animations/KOP interactions are still not functional.
                           Any combo involving crouching or movement is still waiting on the proper funcionality.

//        Current Input: W, W, T
//        Dates 07/19/2017
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To make additional Combos the class must inherent form the Combo1 script
public class Right_Advance : Combo1 {


	void Start ()
    {
		
	}
	

	void Update ()
    {
		
	}
    //Both of the public override functions are nessisary to get the script to work
    public override void DoComboResult()
    {
        Debug.Log("Right Advance Success");
    }

    public override void Initilialize()
    {
     
    }
}
