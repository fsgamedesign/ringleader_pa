﻿//Kevin Hellmuth (kevinsQuest87@gmail.com)

// 7/19/2017
// Nicholas O'Keefe
// This file was only changed to support duplicated scripts
// and should be reverted to the original after testing
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxer_outerSphere1 : MonoBehaviour
{

    [SerializeField]
    private string boxerTag;

    //Opposing boxer's innerSphere script
    private boxer_InnerSphere opposingInner;

    //Boxer Movement as of 06/15/17, only takes movementSpeed var
    private BoxerData1 boxMove_script;

    //Drop the player the OuterSphere is responsible for in the inspector
    public GameObject boxerPosition;

    //used to grab opposing innerSphere script
    public GameObject opposingInnerSphere;

    private bool insideSphere;

    float sphereMoveSpeed;

    //The amount of time it takes before the outerSphere trys to reposition it's self
    [SerializeField]
    float followDelay = 0.05f;
    
    //The Radius of the sphere when it grows after the "encompasTimer" ends
    //NOTE to visually radius changes, select "outerSphere_Influ_Red" in the hierarchy, the gizmo will not resize
    [SerializeField]
    float outerRadius = 2.5f;

    //The Amount of time it takes before the outersphere grows in radius
    //Triggered by OnTriggerEnter of opposing player's innerSphere
    [SerializeField]
    float encompasTimer = 3.0f;



    // Use this for initialization
    void Start()
    {
  
        boxerPosition.GetComponent<Transform>();

        opposingInner = opposingInnerSphere.GetComponent<boxer_InnerSphere>();

        boxMove_script = boxerPosition.GetComponent<BoxerData1>();

        
        sphereMoveSpeed = boxMove_script.moveSpeed / 2f * Time.deltaTime;

    }

    // Update is called once per frame
    void Update()
    {

        if (insideSphere)
        {
 
            //Had to hardcode to adjust movementSpeed
            boxMove_script.moveSpeed = 4f;
        }

        if (!insideSphere)
        {

            boxMove_script.moveSpeed = 7.5f;

        }

    }


    public void OnTriggerExit(Collider other)
    {
        if (other.tag == boxerTag)
        {

            insideSphere = false;
            StartCoroutine(sphereExit());
        }

        if (other.tag == "InnerInflu_Red"|| other.tag == "InnerInflu_Blue")
        {
            
             StopCoroutine(InOpponentInner());
            defaultRadius();
            opposingInner.escapedRadius();

        }
        if (other.tag == "cornerTrig")
        {
            defaultRadius();
        }

        if (other.tag == "railTrig")
        {
            defaultRadius();
        }

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == boxerTag)
        {
            insideSphere = true;
            StopCoroutine(sphereExit());

        }
        if (other.tag == "InnerInflu_Blue")
        {
            StartCoroutine(InOpponentInner());
        }

        if (other.tag=="cornerTrig")
        {
            CornerRadius();
        }

        if (other.tag=="railTrig")
        {
            railLineRadius();
        }

      

    }


    public void CornerRadius() {

        SphereCollider mySphere = transform.GetComponent<SphereCollider>();

        mySphere.radius = .5f;
    }

    public void railLineRadius() {

        SphereCollider mySphere = transform.GetComponent<SphereCollider>();

        mySphere.radius = .8f;

    }

    public void defaultRadius()
    { 
        SphereCollider mySphere = transform.GetComponent<SphereCollider>();

        mySphere.radius = 1.6f;
    }

    IEnumerator sphereExit()
    {
        while (true)
        {
            yield return new WaitForSeconds(followDelay);
            transform.position = Vector3.MoveTowards(transform.position, boxerPosition.transform.position, sphereMoveSpeed);

            if (transform.position == boxerPosition.transform.position)
            {
                break;
            }
        }

    }


    IEnumerator InOpponentInner()
    {
        yield return new WaitForSeconds(encompasTimer);

        SphereCollider mySphere = transform.GetComponent<SphereCollider>();

        mySphere.radius = outerRadius;

        opposingInner.overTaken();

    }

}
