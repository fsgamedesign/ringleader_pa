﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/31/2017
 Credit (online ref./additional authors): 

 Purpose: this is the base class for any in game action that a playable character can do. Any action a player does should
   be its own class that inherits from this one.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Combo : MonoBehaviour {

    public List<string> inputCombination = new List<string>();

    public float inputTiming = 1f;

    public int comboIndexPosition = 0;

    public bool comboValid = false;

    public abstract void Initilialize();

    public abstract void DoComboResult();

    public virtual IEnumerator ComboTimer()
    {
        yield return new WaitForSeconds(inputTiming);

        comboValid = false;
    }

}
