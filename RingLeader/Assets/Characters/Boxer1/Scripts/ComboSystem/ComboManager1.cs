﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/31/2017
 Credit (online ref./additional authors): 

 Purpose: This script manages combo actions attached to a game object. A combo can have 1 input to trigger it or many.
  Make sure that the combo itself is attached to the same game object as this one.
*/

// 7/13/2017
// Nicholas O'Keefe
// Instead of detecting Input by using Input.anykey is now called directly by Boxers/Ref states (currently only the BoxerState_MovePunch & Ref_Movement (07/28/17))
// This allows for controller supported combos
// 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// All combos should be a component attached to the same object as this class.

public class ComboManager1 : MonoBehaviour {

    public List<Combo1> comboList = new List<Combo1>();

    // combos that are valid after the first input will be tracked in this list.
    public List<Combo1> queuedCombos = new List<Combo1>();

    public bool comboHeapActive = false;

    //string keyCode;
    bool noCombos;

    void Awake()
    {
        GetComponents(comboList);

        if(comboList.Count > 0)
        {
            // initialize the combos
            foreach (Combo1 c in comboList)
            {
                c.Initilialize();
            }
        }else
        {
            Debug.Log("No combos were found.");
            noCombos = true;
        }
    }

    // Update is called once per frame
    void Update () {
        if (!noCombos)
        {
            //HandleInput(); // check for input every frame.

            // passively look for invalid combos.
            if (comboHeapActive)
            {
                for (int i = queuedCombos.Count - 1; i >= 0; i--)
                {
                    if (!queuedCombos[i].comboValid)
                    {
                        Debug.Log("Combo for " + queuedCombos[i].GetType().ToString() + " has timed out.");
                        queuedCombos[i].comboIndexPosition = 0;
                        queuedCombos[i].comboValid = false;
                        queuedCombos[i].StopAllCoroutines();
                        queuedCombos.RemoveAt(i);
                    }
                }

                if (queuedCombos.Count == 0)
                {
                    comboHeapActive = false;
                }
            }
        }
	}


    public void HandleInput(string prekey)
    {

        HandleCombo(prekey);

    }


    void HandleCombo(string key)
    {
        Debug.Log(key);
        if (!comboHeapActive) // if there are no combos currently being attempted
        {
            for (int i = 0; i < comboList.Count; i++) // iterate through the combos
            {
                // fastest way to compare strings http://cc.davelozinski.com/c-sharp/fastest-way-to-compare-strings https://msdn.microsoft.com/en-us/library/af26w0wa(v=vs.110).aspx
                if (string.CompareOrdinal(comboList[i].inputCombination[0].ToString(), key) == 0) // does the first command of any given combo match the input string
                {// if true
                    Debug.Log("first combo key valid for " + comboList[i].GetType().ToString());
                    comboHeapActive = true; // there are now combos active
                    queuedCombos.Add(comboList[i]); // add the command to the queued combos
                    queuedCombos[queuedCombos.Count - 1].comboValid = true; // this command is now a valid combo
                    queuedCombos[queuedCombos.Count - 1].StartCoroutine(queuedCombos[queuedCombos.Count - 1].ComboTimer()); // the timer for the combo should start


                    if (queuedCombos[queuedCombos.Count - 1].comboIndexPosition == queuedCombos[queuedCombos.Count - 1].inputCombination.Count - 1) // check if the combo is completed with 1 key
                    { // initiate the command if the combo was completed
                        Debug.Log("Combo achieved" + queuedCombos[queuedCombos.Count - 1].GetType().ToString() + " has been activated.");
                        comboHeapActive = false;
                        queuedCombos[queuedCombos.Count - 1].DoComboResult(); // the command logic
                        ClearComboHeap();
                        break; // exit the for loop since we have completed a combo
                    }

                    // if the first key isn't the only combo key
                    // the position in the combo should increment (ex. if the combo is ctrl e, we have now pressed ctrl and need to look for e)
                    queuedCombos[queuedCombos.Count - 1].comboIndexPosition++;
                }
            }
        }
        else
        {
            // Go through the list of queued combos in reverse order.
            // This is because we need to remove invalid combos from the queue once their timer is up without
            // an input. If we are going forward through the list while removing items from it the index count will 
            // be changing infront of us. If we do it reverse the index count will change with us. 
            // If we are forward iterating (++ instead of --). On index 3 of 5 and need to remove 3 because it is no longer
            // valid. This messes us up because when you remove something at an index in a list it automatically shifts all items
            // positions in the index. If we remove 3, 4 becomes 3, and 5 becomes 4. So when we increment i from 3 to 4 we will skip
            // an item in the list. If we reverse iterate if we are on 3 and need to delete it. 4 becomes 3 and 5 becomes 4. But the next
            // index is 2 which hasn't been moved so we are fine and don't skip any items in the list.
            for (int i = queuedCombos.Count - 1; i >= 0; i--)
            {
                if (comboHeapActive)
                {
                    // is the heap active and is the combo timer for the queued command i still going?
                    if (queuedCombos[i].comboValid)
                    { // if true
                      // Check to see if the input string is equal to the next item in the combo for queued command i
                      // fastest way to compare strings http://cc.davelozinski.com/c-sharp/fastest-way-to-compare-strings https://msdn.microsoft.com/en-us/library/af26w0wa(v=vs.110).aspx
                        if (string.CompareOrdinal(queuedCombos[i].inputCombination[queuedCombos[i].comboIndexPosition].ToString(), key) == 0)
                        {
                            Debug.Log("valid combo key for " + queuedCombos[i].GetType().ToString());
                            if (queuedCombos[i].comboIndexPosition == queuedCombos[i].inputCombination.Count - 1) // combo complete
                            { // initiate the command if the combo was completed
                                Debug.Log("Combo achieved" + queuedCombos[i].GetType().ToString() + " has been activated.");
                                comboHeapActive = false;
                                queuedCombos[i].DoComboResult(); // the command logic
                                ClearComboHeap();
                                break; // exit the for loop since we have completed a combo
                            }
                            else // combo not complete but input was valid
                            {
                                queuedCombos[i].comboIndexPosition++;
                                queuedCombos[i].StopAllCoroutines(); // stop that combos combo timer
                                queuedCombos[i].StartCoroutine(queuedCombos[i].ComboTimer()); // restart the combo timer
                            }
                        }
                    }
                    else // if the command is not valid but the combo heap is still active (end queuedcommands combo valid if)
                    {
                        Debug.Log("Combo for " + queuedCombos[i].GetType().ToString() + " has timed out.");
                        queuedCombos[i].comboIndexPosition = 0; // reset the combo index for that command
                        queuedCombos[i].comboValid = false;
                        queuedCombos[i].StopAllCoroutines();
                        queuedCombos.RemoveAt(i); // remove the item that isn't valid.
                    }
                }
                else // combo heap is no longer active 
                {
                    ClearComboHeap();
                    break; // leave the for loop
                }
            }
        }
    }


    void ClearComboHeap()
    {
        for (int x = queuedCombos.Count - 1; x >= 0; x--)
        {
            queuedCombos[x].comboIndexPosition = 0; // reset all combo positions
            queuedCombos[x].comboValid = false;
            queuedCombos[x].StopAllCoroutines(); // stop any coroutines
        }
        queuedCombos.Clear(); // clear the queue
    }
}
