﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Jab : BoxerState_Basic
{
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        if (bSM.boxerAnimator.GetBool("isCrouching"))
            punchCode = 61;
        else
            punchCode = 1;
        bSM.staminaScript.LeftJab();
        comboManager.HandleInput(bSM.boxerData.button_Left);
        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
