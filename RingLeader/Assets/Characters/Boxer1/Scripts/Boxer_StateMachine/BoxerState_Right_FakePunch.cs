﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Right_FakePunch : BoxerState_Basic{
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        punchCode = 22;
        comboManager.HandleInput(bSM.boxerData.button_Left);
        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
