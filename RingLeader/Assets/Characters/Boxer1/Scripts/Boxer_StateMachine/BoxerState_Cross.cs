﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Cross : BoxerState_Basic
{
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        if (bSM.boxerAnimator.GetBool("isCrouching"))
            punchCode = 62;
        else
            punchCode = 2;
        bSM.staminaScript.RightJab();
        comboManager.HandleInput(bSM.boxerData.button_Right);
        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}