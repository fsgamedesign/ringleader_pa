﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Left_Hook : BoxerState_Basic {
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        if (bSM.boxerAnimator.GetBool("isCrouching"))
            punchCode = 641;
        else
            punchCode = 41;
        bSM.staminaScript.UseHook();
        comboManager.HandleInput(bSM.boxerData.button_Left);
        comboManager.HandleInput(bSM.boxerData.button_Hook);
        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
