﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 7/19/2017
// Nicholas O'Keefe
// Now uses Input Axes and as a result all GetKey's have been replaced with GetButton's
// This will only work with the up to date Input Manager
// To check this goto Edit->ProjectSettings->Input, where as of (07/21/17) there should be a total of 48 Input Axes
//
// Supports Keyboard and Mouse, and Xbox 360 controller though it still needs polish 
//

public class BoxerState_Blocking1 : ByTheTale.StateMachine.State, IMovement
{
    public BoxerStateMachine1 bSM { get { return (BoxerStateMachine1)machine; } }

    Transform boxerTransform;
    string horizontalAxis;
    string verticalAxis;
    CharacterController characterController;
    GameObject enemyObject;

    Vector3 moveDirection = Vector3.zero;

    public override void Initialize()
    {
        base.Initialize();
        boxerTransform = bSM.boxerData.transform;
        horizontalAxis = bSM.boxerData.horizontalAxis;
        verticalAxis = bSM.boxerData.verticalAxis;
        characterController = bSM.characterController;
        enemyObject = bSM.boxerData.enemyObj;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Execute()
    {
        base.Execute();
        boxerTransform.LookAt(enemyObject.transform);
        HandleMovement();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public void HandleMovement()
    {   
        if (Input.GetButton(bSM.boxerData.button_Crouch))
        {
            bSM.boxerAnimator.SetBool("isCrouching", true);
        }
        else
        {
            bSM.boxerAnimator.SetBool("isCrouching", false);
        }

        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
        moveDirection = boxerTransform.TransformDirection(moveDirection);
        moveDirection *= bSM.boxerData.moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        boxerTransform.position = new Vector3(boxerTransform.position.x, 0, boxerTransform.position.z); //Keeps the boxer's feet on the boxing canvas.

        MovementAnimation(moveDirection); //Trigger Animation SM
    }

    //Triggers animations on the current boxer depending on the movement
    public void MovementAnimation(Vector3 _moveDirection)
    {
        bSM.boxerAnimator.SetFloat("Speed", _moveDirection.magnitude);
        _moveDirection.Normalize();
        bSM.boxerAnimator.SetFloat("MovementX", _moveDirection.x);
        bSM.boxerAnimator.SetFloat("MovementZ", _moveDirection.z);

        if (Mathf.Abs(_moveDirection.normalized.magnitude) >= 0.1f)
            Debug.LogError("Animator: x" + _moveDirection.x + " z:" + _moveDirection.z + ". speed " + bSM.boxerAnimator.GetFloat("Speed"));
    }
}