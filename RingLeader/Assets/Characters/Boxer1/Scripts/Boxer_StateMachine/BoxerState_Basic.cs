﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Basic : ByTheTale.StateMachine.State {

    protected Transform boxerTransform;

    protected GameObject enemyObject;
    protected ComboManager1 comboManager;
    protected int punchCode = 0;
    protected ANY_KOP KopScript;
    public BoxerStateMachine1 bSM { get { return (BoxerStateMachine1)machine; } }
    public override void Initialize()
    {
        base.Initialize();
        boxerTransform = bSM.boxerData.transform;
        enemyObject = bSM.boxerData.enemyObj;
        comboManager = bSM.boxerData.comboManager;
        KopScript = bSM.KopSystem;
    }

    public override void Enter()
    {
        base.Enter();
        punchCode = 0;
    }

    public override void Execute()
    {
        base.Execute();
        boxerTransform.LookAt(enemyObject.transform);
        bSM.MovePunch();
    }

    public override void Exit()
    {
        base.Exit();
        punchCode = 0;
    }
}
