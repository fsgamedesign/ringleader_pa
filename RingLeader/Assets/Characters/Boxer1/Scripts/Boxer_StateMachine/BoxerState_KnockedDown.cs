﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_KnockedDown : ByTheTale.StateMachine.State
{
	public BoxerStateMachine1 bSM { get { return (BoxerStateMachine1)machine; } }

	bool Key1Press = false;

	bool Key2Press = false;

	bool Key3Press = false;

	bool Key4Press = false;

	int TimesEntered = 0;

	int A1 = 0;

	int A2 = 0;

	int A3 = 0;

	int A4 = 0;

	public override void Initialize()
	{
		base.Initialize();
	}

	public override void Enter()
	{
		base.Enter();

		CommandManager.instance.Command_KOStart(bSM);

		A1 = 0;

		A2 = 0;

		A3 = 0;

		A4 = 0;

		TimesEntered++;

		Key1Press = false;

		Key2Press = false;

		Key3Press = false;

		Key4Press = false;

		if (GetMachine<BoxerStateMachine1> ().gameObject.name == "Boxer_Red") {

			GameStateManager.gameStateManager.redBoxer++;
		}

		else{
			GameStateManager.gameStateManager.blueBoxer++;
		}

		Debug.Log (TimesEntered);
	}

	public override void Execute()
	{
		base.Execute();

		if (Input.GetKeyDown(KeyCode.V))
		{
			A1++;
			Debug.Log("V has been pressed.");
			if (A1 >= TimesEntered) {
				Key1Press = true;  
				Debug.Log ("<color=red>DONE</color>");
			}

		}

		if (Input.GetKeyDown(KeyCode.B))
		{
			A2++;
			Debug.Log("B has been pressed.");
			if (A2 >= TimesEntered) {
				Key2Press = true;  
				Debug.Log ("<color=red>DONE</color>");
			} 
		}

		if (Input.GetKeyDown(KeyCode.N))
		{
			A3++;
			Debug.Log("N has been pressed.");
			if (A3 >= TimesEntered) {
				Key3Press = true;  
				Debug.Log ("<color=red>DONE</color>");
			}  
		}

		if (Input.GetKeyDown(KeyCode.M))
		{
			A4++;
			Debug.Log("M has been pressed.");
			if (A4 >= TimesEntered) {
				Key4Press = true;  
				Debug.Log ("<color=red>DONE</color>");
			}
		}

		if (Key1Press == true && Key2Press == true && Key3Press == true && Key4Press == true)
		{
			Debug.Log("Standing up");
			bSM.StandUp();         
		}
	}

	public override void Exit()
	{
		base.Exit();
	}
}