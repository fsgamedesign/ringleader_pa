﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Left_Uppercut : BoxerState_Basic {
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        if (bSM.boxerAnimator.GetBool("isCrouching"))
            punchCode = 651;
        else
            punchCode = 51;
        bSM.staminaScript.UseUppercut();
        comboManager.HandleInput(bSM.boxerData.button_Left);
        comboManager.HandleInput(bSM.boxerData.button_Uppercut);
        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
