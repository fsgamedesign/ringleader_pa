﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Right_Uppercut : BoxerState_Basic {
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
        if (bSM.boxerAnimator.GetBool("isCrouching"))
            punchCode = 652;
        else
            punchCode = 52;
        bSM.staminaScript.UseUppercut();
        comboManager.HandleInput(bSM.boxerData.button_Right);
        comboManager.HandleInput(bSM.boxerData.button_Uppercut);
        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
