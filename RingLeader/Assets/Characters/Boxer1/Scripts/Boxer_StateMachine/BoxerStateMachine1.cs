﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 7/13/2017
// Nicholas O'Keefe
// This file was only changed to support duplicated scripts
// and should be reverted to the original after testing
//
// 8/23/2017
// Cheng-An Lin
// Now each punchese has it's own state.


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxerData1))]
[RequireComponent(typeof(CharacterController))]

public class BoxerStateMachine1 : ByTheTale.StateMachine.MachineBehaviour
{
    [HideInInspector] public Animator boxerAnimator;
    [HideInInspector] public Animator ringAnimator;
    [HideInInspector] public BoxerData1 boxerData;
    [HideInInspector] public CharacterController characterController;

    [HideInInspector] public ANY_KOP KopSystem;

    // Allowing the Boxers to know about the GameStateManager
    [HideInInspector] public GameStateManager gSM;

    [HideInInspector] public Stamina staminaScript;
    // ^Added by Daniel Coyne

    //MZ_RingLeader
    public GameObject Ring;
    

    public override void AddStates()
    {
        AddState<BoxerState_MovePunch1>();
        AddState<BoxerState_Blocking1>();
        AddState<BoxerState_Commanded>();
        AddState<BoxerState_Dodging>();
        AddState<BoxerState_Hugging>();
        AddState<BoxerState_KnockedDown>();
        AddState<BoxerState_Paused>();
        AddState<BoxerState_Jab>();
        AddState<BoxerState_Cross>();
        AddState<BoxerState_Left_Hook>();
        AddState<BoxerState_Right_Hook>();
        AddState<BoxerState_Left_Uppercut>();
        AddState<BoxerState_Right_Uppercut>();
        AddState<BoxerState_Left_FakePunch>();
        AddState<BoxerState_Right_FakePunch>();

        SetInitialState<BoxerState_Paused>();
    }

    public void Awake()
    {
        // Initializing the Game State Manager
        gSM = GameObject.FindObjectOfType<GameStateManager>();
        //////////////////////////////////////////////////////

        boxerAnimator = GetComponent<Animator>();
        ringAnimator = Ring.GetComponent<Animator>();
        boxerData = GetComponent<BoxerData1>();
        characterController = GetComponent<CharacterController>();
        staminaScript = GetComponent<Stamina>();
    }
    public override void LateUpdate()
    {
        base.LateUpdate();
    }
    public void KnockDown()
    {
        if (ringAnimator.GetBool("RopesMove") == false);
        {
            ringAnimator.SetBool("RopesMove", true);
            boxerAnimator.SetTrigger("isKD");
            ChangeState<BoxerState_KnockedDown>();
        }

    }


    // Santiago Castaneda / Ring Leader August Version: Adding an Stand Up function 
    // to allow the player to Stand up after being Knock Down
    public void StandUp()
    {
        // This space is to activate the animation to Stand up
        // Right now it uses a simple trigger (it can be changed at any time).
        boxerAnimator.SetTrigger("isUP");
        //

        // Changing the Boxer State to the proper State to Fight

        // This function needs improvement.

        AllowBoxerToFightAfterStandingCountState(gSM.BoxerInput);

        AllowBoxerToFightAfterStandingCountState(gSM.BoxerInput_2);

        gSM.ChangeGameState(Constants.GameState.RoundAction);
    }

    void AllowBoxerToFightAfterStandingCountState(GameObject BoxerGameObject) {

        BoxerGameObject.GetComponent<BoxerStateMachine1>().ChangeState<BoxerState_MovePunch1>();

        BoxerGameObject.GetComponent<ANY_KOP>().IsKD = true;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    //4 YikunGuo
    public void Blocking()
    {
        ChangeState<BoxerState_Blocking1>();
    }
    public void MovePunch()
    {
        ChangeState<BoxerState_MovePunch1>();
    }
    public void Dodge()
    {
        ChangeState<BoxerState_Dodging>();
    }
    public void Jab()
    {
        ChangeState<BoxerState_Jab>();
    }
    public void Cross()
    {
        ChangeState<BoxerState_Cross>();
    }
    public void LeftHook()
    {
        ChangeState<BoxerState_Left_Hook>();
    }
    public void RightHook()
    {
        ChangeState<BoxerState_Right_Hook>();
    }
    public void LeftUppercut()
    {
        ChangeState<BoxerState_Left_Uppercut>();
    }
    public void RightUppercut()
    {
        ChangeState<BoxerState_Right_Uppercut>();
    }
    public void LeftFakePunch()
    {
        ChangeState<BoxerState_Left_FakePunch>();
    }
    public void RightFakePunch()
    {
        ChangeState<BoxerState_Right_FakePunch>();
    }


    #region Animation Event Methods
    public void ToggleIsPunching(int i)
    {
        if (i == 0) {
            boxerData.isPunching = false;
        } else {
            boxerData.isPunching = true;
        }
    }

    public void RightGlove_Damage_Enable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.RightGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }
    public void RightGlove_Damage_Disable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.RightGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }

    public void LeftGlove_Damage_Enable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.LeftGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }
    public void LeftGlove_Damage_Disable(AnimationEvent aniEvent)
    {
        int _Trigger = aniEvent.intParameter;
        float _TKOP = aniEvent.floatParameter;
        boxerData.LeftGlove.SetIsTriggerChecking(_Trigger, _TKOP);
    }

    public void Glove_Block_Enable()
    {
        boxerData.isBlockEnabled = true;
    }
    public void Glove_Block_Disable()
    {
        boxerData.isBlockEnabled = false;
    }

    public void ClearPunchCode()
    {
        boxerAnimator.SetInteger("PunchCode", 0);
    }
    #endregion
}