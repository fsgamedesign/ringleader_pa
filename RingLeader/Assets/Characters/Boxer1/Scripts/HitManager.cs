﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitManager : MonoBehaviour {
	public static HitManager instance;

    public ANY_KOP KOPManager;
	void Awake(){
		instance = this;
	}
	public void hitLanded(BoxerData1 attacker, BoxerData1 target, float _TKOPfromGlove){
		string attackerName = attacker.gameObject.name;
		string targetName = target.gameObject.name;
		Debug.Log (attackerName + " hit "+ targetName);
        KOPManager = target.GetComponent<ANY_KOP>();
        SetKop(_TKOPfromGlove);
        KOPManager.RandomKDCheck();   
	}
    public void SetKop(float _TKOP)
    {
        KOPManager.ModKOP(_TKOP);
    }
}
