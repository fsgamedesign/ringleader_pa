﻿/*        
//        Developer Name: Joshua Claassen
//         Contribution: Basic KOP system with permanent KOP and temorary KOP that is reduced over time. 
//                Feature - KOP for the boxers and when they get hit.
//                Start & End dates 6/14/2017 - 6/15/2017
//                References: looking into coroutine for the temporary KOP countdown
//                        Links:https://docs.unity3d.com/ScriptReference/Coroutine.html
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KOP : MonoBehaviour {

    private int permKOP, tempKOP;
    private float curPermKOP, curTempKOP;

    [SerializeField]
    int permKOPIncrease = 5, tempKOPIncrease = 5;

    [SerializeField]
    int secondsToWait = 1;

    [SerializeField]
    Image PermBar, TempBar;

    [SerializeField]
    int permJabKOP, tempJabKOP, permCrossKOP, tempCrossKOP, permHookKOP, tempHookKOP, permUpperCutKOP, tempUpperCutKOP, permAllInKOP, tempAllInKOP;

    private string fightReadOut;

    private bool falling = false;

    private bool fightOver = false;

	// Use this for initialization
	void Start () {
        permKOP = 0;
        tempKOP = 10;
        StartCoroutine("DecrementTempKOP");
	}
	
	// Update is called once per frame
	void Update () {
        KOPClamp();
        ButtonTests();
        HandleKOPBar();
        GotPunched();
        Knockdown();
        BoxerKnockedDown();
	}

    IEnumerator DecrementTempKOP()
    {
        yield return new WaitForSeconds(secondsToWait);
        if (tempKOP > permKOP)
        {
            tempKOP--;
            Debug.Log(tempKOP + " temp KOP");
        }

        StartCoroutine("DecrementTempKOP");
    }

    private void KOPClamp()
    {
        tempKOP = Mathf.Clamp(tempKOP, permKOP, 100);
        permKOP = Mathf.Clamp(permKOP, 0, 100);
    }

    private void ButtonTests()
    {
        if (gameObject.name == "BoxerRed")
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                permKOP += permKOPIncrease;
                tempKOP += tempKOPIncrease + permKOPIncrease;
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                permKOP -= permKOPIncrease;
                tempKOP -= tempKOPIncrease + permKOPIncrease;
            }
        }
        else if (gameObject.name == "BoxerBlue")
        {
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                permKOP += permKOPIncrease;
                tempKOP += tempKOPIncrease + permKOPIncrease;
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                permKOP -= permKOPIncrease;
                tempKOP -= tempKOPIncrease + permKOPIncrease;
            }
        }
    }

    private void HandleKOPBar()
    {
        curPermKOP = permKOP;
        curTempKOP = tempKOP;

        float permKopAmount = (curPermKOP / 100);

        SetPermBar(permKopAmount);

        float tempKOPAmount = (curTempKOP / 100);
        SetTempBar(tempKOPAmount);
    }

    private void SetPermBar(float myPermKOP)
    {
        PermBar.fillAmount = myPermKOP;
    }

    private void SetTempBar(float myTempKOP)
    {
        TempBar.fillAmount = myTempKOP;
    }

    void GotPunched()
    {
        if (gameObject.name == "BoxerRed")
        {
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                permKOP += permJabKOP;
                tempKOP += permJabKOP + tempJabKOP;
                fightReadOut += "BlueBoxer Jab, ";
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                permKOP += permCrossKOP;
                tempKOP += permCrossKOP + tempCrossKOP;
                fightReadOut += "BlueBoxer Cross, ";
            }
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                permKOP += permHookKOP;
                tempKOP += permHookKOP + tempHookKOP;
                fightReadOut += "BlueBoxer Hook, ";
            }
            if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                permKOP += permUpperCutKOP;
                tempKOP += permUpperCutKOP + tempUpperCutKOP;
                fightReadOut += "BlueBoxer UpperCut, ";
            }
            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                permKOP += permAllInKOP;
                tempKOP += permAllInKOP + tempAllInKOP;
                fightReadOut += "BlueBoxer AllInPunch, ";
            }
        }

        if (gameObject.name == "BoxerBlue")
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                permKOP += permJabKOP;
                tempKOP += permJabKOP + tempJabKOP;
                fightReadOut += "RedBoxer Jab, ";
            }
            if (Input.GetKeyDown(KeyCode.U))
            {
                permKOP += permCrossKOP;
                tempKOP += permCrossKOP + tempCrossKOP;
                fightReadOut += "RedBoxer Cross, ";
            }
            if (Input.GetKeyDown(KeyCode.I))
            {
                permKOP += permHookKOP;
                tempKOP += permHookKOP + tempHookKOP;
                fightReadOut += "RedBoxer Hook, ";
            }
            if (Input.GetKeyDown(KeyCode.O))
            {
                permKOP += permUpperCutKOP;
                tempKOP += permUpperCutKOP + tempUpperCutKOP;
                fightReadOut += "RedBoxer UpperCut, ";
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                permKOP += permAllInKOP;
                tempKOP += permAllInKOP + tempAllInKOP;
                fightReadOut += "RedBoxer AllInPunch, ";
            }
        }
    }

    void Knockdown()
    {
        if (permKOP == 100)
        {
            if (fightOver == false)
            {
                Debug.Log(fightReadOut);
                fightOver = true;
            }
        }
    }

    void BoxerKnockedDown()
    {
        if (fightOver)
        {
            if (gameObject.name == "BoxerRed")
            {
                gameObject.transform.localRotation = Quaternion.Euler(-90, 45, 0);
            }
            if (gameObject.name == "BoxerBlue")
            {
                gameObject.transform.localRotation = Quaternion.Euler(-90, -135, 0);
            }
        }
    }

}
