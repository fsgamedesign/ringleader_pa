﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 7/13/2017
// Nicholas O'Keefe
// Now uses Input Axes and as a result all KeyCodes have been replaced with strings
// This will only work with the up to date Input Manager
// To check this goto Edit->ProjectSettings->Input, where as of (07/28/17) there should be a total of 50 Input Axes
//
// Now gets access to the ComboManager so that all boxer states are able to pass any input(including controller) to the ComboManager
//
public class BoxerData1 : MonoBehaviour
{
    public bool isSouthpaw;

    [SerializeField] string enemyTag;   
    [HideInInspector] public GameObject enemyObj;
    public GameObject comboManagerLocation;
    [HideInInspector] public ComboManager1 comboManager;

    public BoxerColor boxerColor;
    public enum BoxerColor
    {
        Red,
        Blue
    }

    public float moveSpeed;

    [Header("Input")]
    public string horizontalAxis;
    public string verticalAxis;
    public string button_Left; // "1" on control diagram
    public string button_Right; // "2" on control diagram
    public string button_Defend; // "3" on control diagram
    public string button_Hook; // "A/4" on control diagram
    public string button_Uppercut; // "B/5" on control diagram
    public string button_Crouch; // "C/6" on control diagram
    public string button_Dodge; // Click in left analog

    [HideInInspector] public bool isPunching;
    [HideInInspector] public bool isDamageEnabled_LeftGlove;
    [HideInInspector] public bool isDamageEnabled_RightGlove;
    [HideInInspector] public bool isBlockEnabled;
    [HideInInspector] public bool isDodgeEnabled;
    [HideInInspector] public bool isCrouching;
    public Glove RightGlove;
    public Glove LeftGlove;


    void Awake()
    {
        AssignEnemy();
        GetComboManager();
    }

    void AssignEnemy()
    {
        if (boxerColor == BoxerColor.Red)
        {
            enemyObj = GameObject.FindGameObjectWithTag("BlueBoxer");
        }
        else
        {
            enemyObj = GameObject.FindGameObjectWithTag("RedBoxer");
        }
    }

    void GetComboManager()
    {
        comboManager = comboManagerLocation.GetComponent<ComboManager1>();
    }
}