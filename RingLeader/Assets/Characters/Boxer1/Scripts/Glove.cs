﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 8/14/2017
// Cheng-An Lin
// Now the SetIsTriggerChecking function also takes two parameters to send to HitManager.
// The int is to replace bool because animationevent cannot take bool parameter.
// The float is the TKOP of the punch. To modify it, goes to animation clips to edit the event float.

public class Glove : MonoBehaviour {
	public int enemyLayer;
	BoxerStateMachine1 myBoxer;
    int isTriggerChecking = 0;
    float tTKOP;
	// Use this for initialization
	void Start () {
		myBoxer = GetComponentInParent<BoxerStateMachine1> ();
    }
	void OnTriggerStay(Collider collision){

        if (isTriggerChecking == 1)
        {
            if (collision.gameObject.layer == enemyLayer)
            {
                isTriggerChecking = 0;
                BoxerData1 enemy = myBoxer.boxerData.enemyObj.GetComponent<BoxerData1>();
                HitManager.instance.hitLanded(myBoxer.boxerData, enemy , tTKOP);
            }
        }
	
		//if (other.CompareTag ())myBoxer.boxerData.enemyObj
	}
	// Update is called once per frame
	void Update () {
		
	}
    public void SetIsTriggerChecking(int istrue, float TKOPfromAni)
    {
        isTriggerChecking = istrue;
        tTKOP = TKOPfromAni;
    }

}
