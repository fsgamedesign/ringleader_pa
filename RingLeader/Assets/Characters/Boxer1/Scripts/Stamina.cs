﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*        Developer Name: Joshua Claassen
//         Contribution: Basic system for the boxers stamina and having the punches all take away stamina. boxers have a rest time for when they run negative on their stamina.
//                Feature - Stamina for punching.
//                Start & End dates 6/14/2017 - 6/15/2017
//                References:
//                       Links:
*/

/* Developer Name: Daniel Marino Coyne
 * Last Update: 08-16-17
 * 
 * Notes:
 * I mainly worked on implementing the system that Joshua Claassen started. I also organized the document a bit.
*/


public class Stamina : MonoBehaviour {

    #region Variables
    [SerializeField]
    [Tooltip("Maximum Stamina")]
    public int maxStamina = 100;

    public int minStamina = 0;

    [SerializeField]
    [Tooltip("The current amount of stamina that the player has")]
    // DMC_Branch

    private float curStamina;

    [SerializeField]
    [Tooltip("If the player leans against the ropes their stamina increases faster")]
    private float onRopePlus;

    [Tooltip("True = The player has Stamina, False = The player does not have Stamina")]
    public bool hasStamina;

    [Tooltip("When the player uses stamina, this activates and counts down. When the counter reaches zero the player starts to regain stamina.")]
    [SerializeField]
    float staminaTimer = 5.0f;

    [SerializeField]
    [Tooltip("The timer for how long it takes for the StamTimer to reach zero")]
    float restTimer = 5.0f;

    [SerializeField]
    [Tooltip("Amount of stamina it takes to do the action")]
    int RightjabCost = 10, LeftjabCost = 10, crossCost = 20, hookCost = 25, uppercutCost = 30;

    [Tooltip("Bool used to control when the staminaTimer starts")]
    private bool startStamTime;

    [Tooltip("The highest number that the staminaTimer can reach")]
    private float maxStamTimer = 5;

    [SerializeField]
    [Tooltip("How much stamina the player loses by moving around")]
    public float moveLoss = 1;

    private float subedMovement;

#endregion  
    
    // Use this for initialization
    void Start ()
    {
        //Sets the curStamina and staminaTimer valuse to their Max counterparts.
        curStamina = maxStamina;
        staminaTimer = maxStamTimer;
        hasStamina = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        StaminaDebugLog();

        //If the current Stamina is greater than zero then the player has stamina, 
        //if its less than or equal to zero the player does not have stamina.
        if(curStamina > 0)
        {
            hasStamina = true;
        }
        else if (curStamina < 0)
        {
            startStamTime = true;
            curStamina = 0;
            hasStamina = false;
        }

        if (startStamTime == true)
        {
            staminaTimer -= Time.deltaTime;
        }

        if (staminaTimer > 0)
        {
            startStamTime = true;
            curStamina += Time.deltaTime;
        }

        // If the current stamina is greater than or equal to the max stamina then the current stamina equals the max stamina
        // and it resets the startStamTimer bool to false and the staminaTimer to the maxStamTimer.
        if(curStamina >= maxStamina)
        {
            curStamina = maxStamina;
            startStamTime = false;
            staminaTimer = maxStamTimer;
        }

        NegativeStamina();
	}

    // This is the "On the Ropes" feature, that lets the player recharge their stamina faster if
    // they lean back on the ropes.
    private void OnTriggerStay(Collider other)
    {
        //If the player is on the ropes then the rate at which your stamina builds up speeds up.
        if(other.tag == "railTrig")
        {
            if (startStamTime == true)
            {
                staminaTimer -= Time.deltaTime * onRopePlus;
            } 

            if (staminaTimer <= 0)
            {
                startStamTime = true;
                curStamina += Time.deltaTime * onRopePlus;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "railTrig")
        {
            if (startStamTime == true)
            {
                staminaTimer -= Time.deltaTime;
            }

            if (staminaTimer <= 0)
            {
                startStamTime = true;
                curStamina += Time.deltaTime;
            }
        }
    }

    #region StaminaMoves
    public void RightJab()
    {
        staminaTimer = maxStamTimer;
        if (curStamina >= RightjabCost)
        {
            curStamina -= RightjabCost;
            StartStamTimer();
            StaminaDebugLog();
        }
    }

    public void LeftJab()
    {
        staminaTimer = maxStamTimer;
        if (curStamina >= LeftjabCost)
        {
            curStamina -= LeftjabCost;
            StartStamTimer();
            StaminaDebugLog();
        }
    }

    public void UseCross()
    {
        staminaTimer = maxStamTimer;
        if (curStamina >= crossCost)
        {
            curStamina -= crossCost;
            StartStamTimer();
            StaminaDebugLog();
        }
    }

    public void UseHook()
    {
        staminaTimer = maxStamTimer;
        if (curStamina >= hookCost)
        {
            curStamina -= hookCost;
            StartStamTimer();
            StaminaDebugLog();
        }
    }

    public void UseUppercut()
    {
        staminaTimer = maxStamTimer;
        if (curStamina >= uppercutCost)
        {
            curStamina -= uppercutCost;
            StartStamTimer();
            StaminaDebugLog();
        }
    }

    public void UseAllin()
    {
        curStamina -= maxStamina;
        StartStamTimer();
        StaminaDebugLog();
    }
    #endregion

    #region Get Values

    public float GetCurStamina()
    {
        return curStamina;
    }

    public float MovementLoss()
    {
        return curStamina - 0.1f;
    }

    public int GetMaxStamina()
    {
        return maxStamina;
    }

    private void StartStamTimer()
    {
        startStamTime = true;
        //staminaTimer = maxStamTimer;
    }

    private void StaminaDebugLog()
    {
        //Debug.Log(gameObject.name + " has " + curStamina + " Stamina Left");
    }

    private void NegativeStamina()
    {
        if (curStamina <= 0)
        {
            staminaTimer = restTimer;
        }
    }
    #endregion
}