﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 8/14/2017
// Cheng-An Lin
// Now the KOP system takes one parameter from HitManager to set TKOP
// To modify TKOP from each punch, goes to Glove script file to see instruction.

public class ANY_KOP : MonoBehaviour {
    /* Developer: Alexander Yaws
     * The plan of this script is to make KOP function more like a percentage than hit points.
     * Any hit could spend a boxer to the mat. As a Boxer is hit, their chance to fall down increases. 
     * PKOP = Permanent Knock Out Percentage
     * TKOP = Temporary Knock Out Percentage
     * Quick note for using this in my prototype scene: press Z to test punch, hold X to Clinch, watch the Console for the results.
     */

    BoxerStateMachine1 boxer;
    
    [SerializeField]
    private float maxPKOP = 80f;

    //Storage variables 
    [SerializeField] float TKOP, currentPKOP, KOP;

    //hitKOPis the TKOP and/or PKOP gained from a punch
    [SerializeField]
    float hitPKOP = 5f;

    //bool for if the boxer is knocked down
    private bool isKD;

    //if a punch is KOPossible, it is strong enough that it may knock the boxer down. Idea for later, only test punches right now.
    //private bool KOPossible;
    //Another idea is if a Boxer recieves enough PKOP than it may begin to trigger the knock down.
    //[SerializedField] float KnockDownPKOP;

    //variable to store the random number generated to determine KO and get up
    private float KOroll;

    //The count for the boxer to get back up
    [SerializeField]
    private int refCountingSeconds = 1;

    //The moments in the count the boxer rerolls to try and stand. Every four seconds, the boxer tries to stand.
    [SerializeField]
    private int boxerStandCheck = 4;

    //Ref Counting number storage
    [SerializeField]
    private int refCountNumber = 0;

    //Clinching: on boxer holding onto the other to stop them from punching and to reduce TKOP
    //A Float for amount of TKOP reduction and another for how often.
    [SerializeField]
    private float clinchTKOPRecovery = 2, clinchSeconds = 0.5f;

    [SerializeField]
    private int TKOPRecoverTime = 5;

    //Blocking effects KOP, a bool to check whether the player is blocking or not.
    bool isBlocking;

    // Santiago Castaneda/ Ring Leader August version
    // Adding mutator to interact with the isKD boolean.
    public bool IsKD
    {
        get
        {
            return isKD;
        }

        set
        {
            isKD = value;
        }
    }
    /// <summary>
    /// ////////
    /// </summary>


    // Use this for initialization
    void Start ()
    {
        TKOP = 0f;
        currentPKOP = 0f;
        IsKD = false;
        isBlocking = false;
        StartCoroutine("DoTKOPRecovery");
        boxer = GetComponent<BoxerStateMachine1>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        KOPTest();
        ManageKOP();
    }

    private void KOPTest()
    {
        if (IsKD == false)
        {
            //Check input for test Punching
            if (Input.GetKeyDown(KeyCode.Z))
            {
                ModKOP(3f);
                Debug.Log("Boxer was hit by Test Punch. TKOP=" + TKOP + ", PKOP=" + currentPKOP + ", KOP=" + KOP);
                RandomKDCheck();
            }

            //Check input for test Clinching
            if (Input.GetKeyDown(KeyCode.X))
            {
                Debug.Log("The boxers go into a Clinch");

                StartCoroutine("DoClinchRecovery");
            }
        }
    }

    void PrototypePunch()
    {
        //if collsion of punch is detected, affect KOP by a single value
    }

    public void ModKOP(float _TKOPvalue)
    {
        TKOP = Mathf.Clamp(TKOP + _TKOPvalue, 0f, 100f);
        if (!boxer.boxerData.isBlockEnabled)
        {
            currentPKOP = Mathf.Clamp(currentPKOP + hitPKOP, 0f, 100f);
        }
        KOP = TKOP + currentPKOP;
        Debug.Log("KOP = " + KOP + " TKOP = " + TKOP + " currentPKOP = " + currentPKOP);
    }
    public void RandomKDCheck()
    {
        //roll a random number to check against to determine KO
        KOroll = Random.Range(50, 100);
        Debug.Log("Boxer rolls a " + KOroll + " to try and stay up.");

        //Check the random number against current KOP, 
        //extra check added to allow a constant 1% chance of victory in the case a 100 was rolled
        if (KOP >= 99)
        {
            if (KOroll <= 99)
            {
                ToggleKDBool();
                KnockedDownBoxer();
            }
        }
        else
        {
            if (KOroll <= KOP)
            {
                ToggleKDBool();
                KnockedDownBoxer();
            }
        }
    }

    void ManageKOP()
    {
        //prevent the current permanent from getting higher than the max
        if (currentPKOP > maxPKOP)
        {
            currentPKOP = maxPKOP;
        }
        //prevent the temporary from going less than zero
        if (TKOP < 0)
        {
            TKOP = 0;
        }
        //prevent the current permanent from going less than zero
        if (currentPKOP < 0)
        {
            currentPKOP = 0;
        }
        //Set the KOP
        KOP = TKOP + currentPKOP;
    }
    public void ResetTKOP()
    {
        TKOP = 0;
    }

    void KnockedDownBoxer()
    {   
        Debug.Log(gameObject.name + " is Down!");

        boxer.KnockDown();
        //reset TKOP to zero and recalculate
        ResetTKOP();
        KOP = TKOP + currentPKOP;

        //StartCoroutine("RefCountdownTimer");
        //StartCoroutine("BoxerStandChance");
    }

    void CheckPunch()
    {
        //idea: Check the string name of each punch and assign KOP values to each punch
    }

    public void ToggleKDBool()
    {
        IsKD = !IsKD;
    }

    public bool GetMyKDBool()
    {
        return IsKD;
    }

    public bool GetBlockingBool()
    {
        return isBlocking;
    }

    public void ToggleBlockingBool()
    {
        isBlocking = !isBlocking;
    }

    public float GetTKOP
    {
        get
        {
            return TKOP;
        }
        set
        {
            TKOP = value;
        }
    }

    public float GetCurrentPKOP
    {
        get
        {
            return currentPKOP;
        }
        set
        {
            currentPKOP = value;
        }
    }
    public float GetPunchPKOP
    {
        get
        {
            return hitPKOP;
        }
        set
        {
            hitPKOP = value;
        }
    }

    IEnumerator RefCountdownTimer()
    {
        yield return new WaitForSeconds(refCountingSeconds);
        
        if (IsKD)
        {
            refCountNumber++;

            Debug.Log(refCountNumber + "!");

            if (refCountNumber == 10)
            {
                MatchOver();
            }

            if (refCountNumber < 11)
            {
                StartCoroutine("RefCountdownTimer");
            }
        }
    }

    IEnumerator BoxerStandChance()
    {
        yield return new WaitForSeconds(boxerStandCheck);
        
        //same check as before to see if the boxer gets up.
        if (refCountNumber <= 10)
        {
            //Debug.Log("The Boxer tries to stand!");
            
            //roll a new number to see if they stand up
            KOroll = Random.Range(1, 100);
            Debug.Log("Boxer rolls a " + KOroll + " to try and stand up.");


            if (KOP >= 99)
            {
                if (KOroll <= 99)
                {
                    Debug.Log("The Boxer fails to get up!");

                    StartCoroutine("BoxerStandChance");
                }
                else
                {
                    Debug.Log("The Boxer gets up!");
                    ToggleKDBool();
                    refCountNumber = 0;
                }
            }
            else
            {
                if (KOroll <= KOP)
                {
                    Debug.Log("The Boxer fails to get up!");

                    StartCoroutine("BoxerStandChance");
                }
                else
                {
                    Debug.Log("The Boxer gets up!");
                    ToggleKDBool();
                    refCountNumber = 0;
                }
            }
        }
    }

    IEnumerator DoClinchRecovery()
    {
        yield return new WaitForSeconds(clinchSeconds);

        if(Input.GetKey(KeyCode.X))
        {
            TKOP -= clinchTKOPRecovery;
            ManageKOP();

            Debug.Log("Current TKOP=" + TKOP + ", KOP=" + KOP);

            StartCoroutine("DoClinchRecovery");
        }
    }

    IEnumerator DoTKOPRecovery()
    {
        yield return new WaitForSeconds(TKOPRecoverTime);

        TKOP--;
        ManageKOP();
        //Debug.Log("TKOP = " + TKOP);

        StartCoroutine("DoTKOPRecovery");
    }

    void MatchOver()
    {
        Debug.Log("That'it! The match is over!");

    }
}
