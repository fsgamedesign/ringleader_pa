﻿using UnityEngine;

//using System.Collections;
//using System;

using System.Collections.Generic;

//using System.IO;

public class SkelPosingUtility : MonoBehaviour
{
    public float jointRadius = 0.25f;
    public Transform[] joints;

    [System.Serializable]
    public struct PoseInfo
    {
        public string name;
        public Vector3[] position;
        public Quaternion[] rotation;

        public PoseInfo(int jointCount)
        {
            name = string.Empty;
            position = new Vector3[jointCount];
            rotation = new Quaternion[jointCount];
        }
    }

    //    [System.Serializable]
    //    public struct Poses
    //    {
    //        //[HideInInspector]
    //        public PoseInfo[] bindPose;
    //    }

    //[HideInInspector]
    public PoseInfo bindPose;

    //[HideInInspector]
    public List<PoseInfo> PosesList = new List<PoseInfo>();
    //[HideInInspector]
    //public Poses[] GOPoses;

}
