﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SkelPosingUtility))]
public class SkelPosingUtilityEditor : Editor
{
    SkelPosingUtility poseUtility;
    List<GameObject> selectedJoints;


    bool unfold;
    string poseName;


    private void OnEnable()
    {
        selectedJoints = new List<GameObject>();
        poseUtility = (SkelPosingUtility)target;
    }

    public override void OnInspectorGUI()
    {
        unfold = EditorGUILayout.Foldout(unfold, "Joint Configuration");
        if (unfold)
        {
            base.OnInspectorGUI();


            if (GUILayout.Button("Record Bind Pose", GUILayout.ExpandWidth(false)))
            {
                Undo.RecordObject(poseUtility, "Rotate Joint");
                poseUtility.bindPose = new SkelPosingUtility.PoseInfo(poseUtility.joints.Length);
                poseUtility.bindPose.name = "BindPose";
                for (int i = 0; i < poseUtility.joints.Length; i++)
                {
                    poseUtility.bindPose.position[i] = poseUtility.joints[i].localPosition;
                    poseUtility.bindPose.rotation[i] = poseUtility.joints[i].localRotation;
                }
            }
        }



        poseName = EditorGUILayout.TextField("Pose Name", poseName);
        if (GUILayout.Button("Add Pose") && !string.IsNullOrEmpty(poseName))
        {
            SkelPosingUtility.PoseInfo newPose = new SkelPosingUtility.PoseInfo(poseUtility.joints.Length);
            newPose.name = poseName;
            for (int i = 0; i < poseUtility.joints.Length; i++)
            {
                newPose.position[i] = poseUtility.joints[i].localPosition;
                newPose.rotation[i] = poseUtility.joints[i].localRotation;
            }
            poseUtility.PosesList.Add(newPose);

            Debug.Log(poseUtility.PosesList.Count);
        }





        if (GUILayout.Button("Apply BindPose", GUILayout.ExpandWidth(false)))
        {
            for (int i = 0; i < poseUtility.joints.Length; i++)
            {
                Undo.RecordObject(poseUtility.joints[i], "Apply Bind Pose");
                poseUtility.joints[i].localPosition = poseUtility.bindPose.position[i];
                poseUtility.joints[i].localRotation = poseUtility.bindPose.rotation[i];
            }
        }


        for (int j = 0; j < poseUtility.PosesList.Count; j++)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Apply " + poseUtility.PosesList[j].name, GUILayout.ExpandWidth(false)))
            {
                for (int i = 0; i < poseUtility.joints.Length; i++)
                {
                    Undo.RecordObject(poseUtility.joints[i], "Apply Bind Pose");
                    poseUtility.joints[i].localPosition = poseUtility.PosesList[j].position[i];
                    poseUtility.joints[i].localRotation = poseUtility.PosesList[j].rotation[i];
                }
            }
            if (GUILayout.Button("Delete " + poseUtility.PosesList[j].name, GUILayout.ExpandWidth(false)))
            {
                poseUtility.PosesList.RemoveAt(j);
//                for (int i = 0; i < poseUtility.joints.Length; i++)
//                {
//                    Undo.RecordObject(poseUtility.joints[i], "Apply Bind Pose");
//                    poseUtility.joints[i].localPosition = poseUtility.PosesList[j].position[i];
//                    poseUtility.joints[i].localRotation = poseUtility.PosesList[j].rotation[i];
//                }
            }
            GUILayout.EndHorizontal();
        }
    }

    private void OnSceneGUI()
    {
        Event ev = Event.current;

        if (poseUtility.joints != null)
        {
            for (int i = 0; i < poseUtility.joints.Length; i++)
            {
                Transform child = poseUtility.joints[i];
                Transform parent = child.parent;
                Handles.DrawLine(child.position, parent.position);
                if (parent != null)
                {
                    if (Selection.Contains(poseUtility.joints[i].gameObject))
                    {
                        Handles.color = Color.cyan;
                    }
                    if (Handles.Button(child.position, child.rotation, poseUtility.jointRadius, poseUtility.jointRadius, Handles.SphereHandleCap))
                    {
                        if (!ev.shift)
                        {
                            selectedJoints.Clear();
                        }
                        if (!selectedJoints.Contains(poseUtility.joints[i].gameObject))
                        {
                            selectedJoints.Add(poseUtility.joints[i].gameObject);
                            Selection.objects = selectedJoints.ToArray();
                        }
                    }
                    Handles.color = Color.white;
                }
            }
        }
    }
}
