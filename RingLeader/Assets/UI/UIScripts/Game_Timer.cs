﻿//Aurthor:Ellis Hernandez
//Date: 8/13/2017
//Credit: Ellis Hernandez
//Purpose: Handle GameTime

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game_Timer : MonoBehaviour {

    //Basic Declarables
    [HideInInspector]
    private string minutes; //public minute for HUD use
    [HideInInspector]
    private string seconds; //public seconds for HUD use
    [SerializeField]
    private float RoundClock; //GameTime
    private float MaxRoundClock;
    [SerializeField]
    private bool inPlay;//Is match in motion ? True=fighting / False=Break or between round
    [SerializeField]
    private float BreakClock; //Break time max 1 minute
    [SerializeField]
    private bool Paused; //is the game paused?

    //Audio Declarables
    private AudioSource Audio;
    [SerializeField]
    private AudioClip TenSecondClank;
    [SerializeField]
    private AudioClip KnockDownCount;
    [SerializeField]
    private AudioClip BellSound;

    //Feature Testing Declarables
    [SerializeField]
    private bool BoxerDown; //Is there a boxer knocked down?
    private bool TenSecondSound; //Fix bug for TenSecondClank audio loop


    // Use this for initialization
    void Start () {

        //DontDestroyOnLoad(this); //Needed throughout the game

        //Default Values for game start
        Paused = true;
        inPlay = true;
        BoxerDown = false;
        TenSecondSound = false;
        RoundClock = 177f;
        MaxRoundClock = RoundClock;
        BreakClock = 60;

        //Start Audio
        Audio = GetComponent<AudioSource>();
        HandleSounds(BellSound);

	}
	
	// Update is called once per frame
	void Update () {

        if (!Paused)
        {
            HandleTime();
        }
    }

    void HandleTime() // Covers GameTime and BreakTime
    {

        if (inPlay) 
        {
            //Increment time during play
            RoundClock -= Time.deltaTime;
            minutes = Mathf.Floor(RoundClock / 60).ToString("00");
            seconds = (RoundClock % 60).ToString("00");
            //print(minutes + ":" + seconds); //Testing time format

            //10 Second Clack
            if (RoundClock <= 10.00f && TenSecondSound == false)
            {
                print("10 seconds left");
                HandleSounds(TenSecondClank);
            }

            //reset breaktime
            BreakClock = 60;
        }

        if (!inPlay)
        {
            BreakClock -= Time.deltaTime;

            //Reset RoundClock
            RoundClock = 177f;
        }

    }

    void HandleSounds(AudioClip sound) //Take sound file in and play
    {
        Audio.clip = sound;
        Audio.Play();

        if(sound == KnockDownCount) //stops the audio bug loop // MUST USE "AUDIO.STOP" IN ORDER TO STOP THE SOUND WHEN BOXER GETS UP!!!
        {
            BoxerDown = !BoxerDown;
        }

        if(sound == TenSecondClank)//stops the audio bug loop // MUST USE "AUDIO.STOP" IN ORDER TO STOP THE SOUND!!!
        {
            TenSecondSound = !TenSecondSound;
        }
    }

    public void RestMaxRoundClock() {

        RoundClock = MaxRoundClock;
    }

    //Accessors Below -------------------------------------------------------------------------------------------------------------------------------------------------------------

    public string GetMinutes() //minutes string
    {
        return minutes;
    }
    public string GetSeconds() //seconds string
    {
        return seconds;
    }
    public float GetRoundClock() //RoundClock float
    {
        return RoundClock;
    }
    public bool GetinPlay //inPlay bool
    {
        get{
            return inPlay;
        }

        set {

            inPlay = value;
        }

    }

    public float GetBreakClock() //BreakClock float
    {
        return BreakClock;
    }
    public bool GetPaused //Paused Bool
    {
        get {
            return Paused;
        }

        set {
            Paused = value;
        }
    }

}//EOC
