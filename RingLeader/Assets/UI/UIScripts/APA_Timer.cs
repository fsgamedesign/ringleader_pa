﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APA_Timer : MonoBehaviour
{
    //[HideInInspector]
    // Santiago Castaneda/ Ring Leader August Version:
    // Allowing the developer to see the current Timer
    public float currTimer = 0f;

    [SerializeField]private float maxTimer = 60.0f;
    public bool TimerOn = false;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (TimerOn) {
            currTimer += Time.deltaTime;
        }
	}

    public float GetCurrTimer()
    {
        return currTimer;
    }

    public float GetMaxTimer()
    {
        return maxTimer;
    }
}
