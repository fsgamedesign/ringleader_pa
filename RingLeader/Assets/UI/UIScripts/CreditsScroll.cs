﻿//------------------------------------------------------------------------------------------------
// Author: Tiffani Koczenasz
// Date: 9/24/2017
// Credit: 
// Credit:
// Purpose: This will display the Credits for the game. The credits will scroll up the screen 
//			until all credits have been displayed.
//------------------------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsScroll : MonoBehaviour {

	public Text Credits;
	public Scrollbar scrollBar;
	public ScrollRect scrollRect;

	public void Update(){

		//Scrolls the rect downwards to value of 0
		scrollRect.velocity = new Vector2 (0f, 100f);

		//TODO: Adjust Height of the CreditsText as names are added. 
		//				creditsPanel > ScrollView > Viewport > CreditsText
		//
		//		Replace back button - once all credits have been displayed, switch back to the main menu. 

		//SETUP Info
		//Add new names and categories to this text script to display onto screen
		//
		//Title
		//two empty lines
		//Developers
		//Empty line
		//Then add Feature Name: Student Name
		//Two Empty lines
		//Production Assistants
		//Empty line
		//Then add Feature Name: Student Name
		//Two Empty lines
		//Special Thanks
		//Empty line
		//Then add just Student Name
		//Two Empty lines
		Credits.text = 
			"\n" +
			"\n" +
			"\n Ring Leader" +
			"\n" +
			"\n" +
			"\nProduction Assistants" +
			"\n" +
			"\nFeatureWork:Name" +
			"\nFeatureWork:Name" +
			"\nFeatureWork:Name" +
			"\n" +
			"\n" +
			"\nDevelopers" +
			"\n" +
			"\nFeatureWork:Name" +
			"\nFeatureWork:Name" +
			"\nFeatureWork:Name" +
			"\n" +
			"\n" +
			"\nSpecial Thanks" +
			"\n" +
			"\nName" +
			"\nName" +
			"\nName";
	} //End Update
} //End CreditsScroll
