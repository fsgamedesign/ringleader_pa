﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FocusMode_Panel : MonoBehaviour {

    private float trackTime;
    public float TrackTime() { return trackTime; }

    private Color defaultcolor = Color.white;

    public string _Tag;

    RefereeStateMachine Focusm;

    // For future reference
    // Q means Referee...
    public GameObject Q;

    private Dictionary<changeStates, Action> fsm = new Dictionary<changeStates, Action>();

    enum changeStates
    {
        ON,
        OFF,

        NUM_STATES
    }

    private changeStates curState = changeStates.ON;

    
    void Start()
    {
        Focusm = Q.GetComponent<RefereeStateMachine>();//access to another script
        fsm.Add(changeStates.ON, new Action(StateOn));
        fsm.Add(changeStates.OFF, new Action(StateOFF));
        
        ResetTimeSinceLastTransition();

    }

    void SetState(changeStates newState)
    {
        curState = newState;
    }

   
    void Update()
    {
        
        fsm[curState].Invoke();
        trackTime += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
      
        if (other.tag == _Tag)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.green;

            ResetTimeSinceLastTransition();
            SetState(changeStates.ON);
          
        }
    }

    void StateOn()
    {
        if (Focusm.checking == true)
        {
            ResetTimeSinceLastTransition();
            SetState(changeStates.OFF);
        }

        else
            if (trackTime > 5.0f )
            {
                ResetTimeSinceLastTransition();
                gameObject.GetComponent<Renderer>().material.color = defaultcolor;

            }

             
    }

    void StateOFF()
    {
        if (trackTime > 5.0f)
        {
            gameObject.GetComponent<Renderer>().material.color = defaultcolor;
            ResetTimeSinceLastTransition();
           
        }
    }



    public void ResetTimeSinceLastTransition()
    {
        trackTime = 0;
    }

}

