﻿/*
 -----------------------------------------------------------------
 Author: Santiago Castaneda Munoz
 Date: XX-XX-XXX
 Credit:
 Purpose:
 -----------------------------------------------------------------
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWorkIsDone : MonoBehaviour {

    [SerializeField]
    RoundStart RefRS;

    public void CameraWorkHasBeenCompleted()
    {
        RefRS.GetCameraWorkDone += 1;
    }
}
