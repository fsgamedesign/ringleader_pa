﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Author: Casey Howard(August, 12) / (Add future authors here)
 *  Date: 8/13/2014
 *  Title: CameraWork logic/Variables
 *  Purpose: To add and state the basic variables and functions needed for the state machine.
 *  Credit: Casey Howard
 */
public class CameraWorkEarly : MonoBehaviour {

    //Float used to transition between cameras using time.
    private float CurrentTime;
   
    

    //Method for returning the current time to be used across inheritable classes.
    public float CT() { return CurrentTime; }
    
    //Function for establishing how the current time increases. 
    void CTime()
    {
        CurrentTime += 1 * Time.deltaTime;
    }

    public void ResetCamera()
    {
        CurrentTime = 0;
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        CTime();
	}
}
