﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
public class CameraWork_StateMachineEarly : CameraWorkEarly
{
 /*
 *  Author: Casey Howard
 *  Date: 8/13/2014
 *  Title: Ringleader CameraWork StateMachine
 *  Purpose: To add and state the basic variables and functions needed for the state machine.
 *  Credit: Casey Howard
 */
 // Accessor for Parent script and it's functions/variables
    private CameraWorkEarly Target;
    
    // Declare vairables here to add a new camera, C = camera
    [SerializeField]
    Camera MainC;
    [SerializeField]
    Camera SecC;
    [SerializeField]
    Camera ThrC;
    [SerializeField]
    Camera FourC;
    [SerializeField]
    Camera RC;

    //Player cameras
    [SerializeField]
    Camera Player1;
    [SerializeField]
    Camera Player2;

    //Animator accessor
    [SerializeField]
    Animator Transit;

    [SerializeField]
    Animator Transit2;

    [SerializeField]
    Animator Transit3;

    // Use this for initialization
    void Start () {
        Target = GameObject.Find("Camera").GetComponent<CameraWorkEarly>();

        Transit = FindObjectOfType<Camera>().GetComponent<Animator>();

        //Setting up what cameras are on and off during the beginning of runtime
        MainC.GetComponent<Camera>().enabled = true;
        SecC.GetComponent<Camera>().enabled = false;
        ThrC.GetComponent<Camera>().enabled = false;
        FourC.GetComponent<Camera>().enabled = false;
        RC.GetComponent<Camera>().enabled = false;
        Player1.GetComponent<Camera>().enabled = false;
        Player2.GetComponent<Camera>().enabled = false;

        Transit.SetBool("CamGO",false);

        Transit2.SetBool("RcamGO", false);

        Transit3.SetBool("TcamGO",false);
    }

    //Test for Transitions/Movement/Rotation of Cameras during runtime. 
    void CameraTransition()
    {
        if (Target.CT() >= 1.5)
        {
            Transit.SetBool("CamGO", true);
        }
        if (Target.CT() >= 2.9)
        {
            MainC.enabled = false;
            SecC.enabled = true;
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 4)
        {
            SecC.enabled = false;
            ThrC.enabled = true;
            Transit3.SetBool("TcamGO", true);
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 9)
        {
            ThrC.enabled = false;
            FourC.enabled = true;
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 10.5)
        {
            FourC.enabled = false;
            RC.enabled = true;
            Transit2.SetBool("RcamGO", true);
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 15)
        {
            MainC.enabled = true;
            RC.enabled = false;
            Transit2.SetBool("RcamGO", false);
            Transit3.SetBool("TcamGO", false);
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 18)
        {
            MainC.enabled = false;
            Player1.enabled = true;
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 21)
        {
            Player1.enabled = false;
            Player2.enabled = true;
            Debug.Log("Cam Change");
        }
        if (Target.CT() >= 24)
        {
            Target.ResetCamera();
            Debug.Log("Reset");
        }

    }
	// Update is called once per frame
	void Update () {

        CameraTransition();

	}
}
