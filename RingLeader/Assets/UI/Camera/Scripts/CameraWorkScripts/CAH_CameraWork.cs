﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAH_CameraWork : MonoBehaviour {

    [SerializeField]
    Camera MainC;
    [SerializeField]
    Camera SecC;
    [SerializeField]
    Camera ThrC;
    [SerializeField]
    Camera FourC;
    [SerializeField]
    Camera RingC;
    //Player cameras
    [SerializeField]
    Camera Player1;
    [SerializeField]
    Camera Player2;

    //Animator accessor
    [SerializeField]
    Animator Transit;
    [SerializeField]
    Animator Transit2;
    [SerializeField]
    Animator Transit3;

    //------CameraTimer------------
    [SerializeField]
    private float CameraSetTime;
    public float NormalTime() { return CameraSetTime; }
    //Keeps track of time-------
    private float CameraTimer;
    public float CamT() { return CameraTimer; }
    public void ResetCT() { CameraTimer = 0; }
    //--------------------------
    //-----------------------------
    // Use this for initialization
    void Start()
    {
        //--Enable Main Camera on start-------------
        MainC.GetComponent<Camera>().enabled = true;
        SecC.GetComponent<Camera>().enabled = false;
        ThrC.GetComponent<Camera>().enabled = false;
        FourC.GetComponent<Camera>().enabled = false;
        RingC.GetComponent<Camera>().enabled = false;
        Player1.GetComponent<Camera>().enabled = false;
        Player2.GetComponent<Camera>().enabled = false;
        //------------------------------------------
        StartingRound();
        //----Camera Animations------
        Transit.SetBool("CamGO", false);
        Transit2.SetBool("RcamGO", false);
        Transit3.SetBool("TcamGO", false);
        //---------------------------
    }

    public void StartingRound() {GetComponent<CAH_CameraWorkStateMachine>().ChangeState<CameraChangeState>(); }

    public void MainCamZoomOut()
    { Transit.SetBool("CamGO", true);
        print("Main Camera On");
    }

    public void BeginCameraTransitions()
    { Transit2.SetBool("RcamGO", true);
        print("Ring Camera On");
        MainC.GetComponent<Camera>().enabled = false;
        RingC.GetComponent<Camera>().enabled = true;
    }

    public void SecondCameraTransition()
    { Transit3.SetBool("TcamGO", true);
        print("Second Camera On");
        RingC.GetComponent<Camera>().enabled = false;
        SecC.GetComponent<Camera>().enabled = true;
    }
    public void Player1CameraTransition()
    {
        SecC.GetComponent<Camera>().enabled = false;
        Player2.GetComponent<Camera>().enabled = false;
        Player1.GetComponent<Camera>().enabled = true;
    }

    public void Player2CameraTransition()
    {
        Player1.GetComponent<Camera>().enabled = false;
        Player2.GetComponent<Camera>().enabled = true;
    }
    public void LastCameraTransition()
    {
        Transit.SetBool("CamGO", true);
        Transit2.SetBool("RcamGO", false);
        Transit3.SetBool("TcamGO", false);
        SecC.GetComponent<Camera>().enabled = false;
        MainC.GetComponent<Camera>().enabled = true;
    }


    // Update is called once per frame
    void Update()
    {
        CameraTimer += 1 * Time.deltaTime;
        //Debug.Log(CameraTimer);
    }
}
