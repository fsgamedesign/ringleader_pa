﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAH_CameraWorkStateMachine : ByTheTale.StateMachine.MachineBehaviour {

    public override void AddStates()
    {
        AddState<MainCameraState>();
        AddState<CameraChangeState>();
        AddState<FocusTimeState>();
        AddState<CornerTimeState>();
        SetInitialState<MainCameraState>();
    }
}

public class CameraStates : ByTheTale.StateMachine.State
{
    protected CAH_CameraWork CW;
    protected RoundStart RefRS;

    public override void Enter()
    {
        CW = GetMachine<CAH_CameraWorkStateMachine>().GetComponent<CAH_CameraWork>();
        RefRS = GameObject.FindObjectOfType<RoundStart>();
        CW.ResetCT();
    }
    
}

public class MainCameraState : CameraStates
{

    public override void Enter()
    {
        base.Enter();
        RefRS.GetCameraWorkDone = 1;
        RefRS.DoComboResult();
        CW.LastCameraTransition();
    }
    public override void Execute()
    {
        //------Corner Time Camera-----
        //if(){ }
        //-----------------------------

        //------Focus Time Cameras-----
        //if(){ }
        //-----------------------------
    }
    public override void Exit()
    {

    }
}

public class CameraChangeState : CameraStates
{
    public override void Enter()
    {
        base.Enter();
        CW.MainCamZoomOut();
    }
    public override void Execute()
    {
        if ((CW.CamT()) >= 3) { CW.BeginCameraTransitions(); }
        if ((CW.CamT()) >= 6) { CW.SecondCameraTransition(); }
        if ((CW.CamT()) >= 8.5) { CW.Player1CameraTransition(); }
        if ((CW.CamT()) >= 10.5) { CW.Player2CameraTransition(); }
        if ((CW.CamT()) >= 12) { machine.ChangeState<MainCameraState>(); }
    }
    public override void Exit()
    {   
    }
}
public class FocusTimeState : CameraStates
{

}
public class CornerTimeState : CameraStates
{

}